#include "autocellexception.h"
#include "automatespecifier.h"

AutomateSpecifier::AutomateSpecifier(std::string nom, unsigned int Min, unsigned int Max, unsigned int Vie, std::string strRegleJDV, unsigned int num, bool N, bool S,bool E, bool O):nomAutomate(nom) ,nbMin(Min), nbMax(Max), nbVie(Vie), strRegle(strRegleJDV), numRegle(num),ventNord(N), ventSud(S), ventOuest(O), ventEst(E)
{
    if(nbMin>nbMax)
        throw AutoCellException("Nombre de voisins minimum superieur au nombre de voisin maximum");
    if(num<0 || num>256)
         throw AutoCellException("le numéro de la règle doit être compris entre 0 et 255");
}
