#ifndef SOMMAIRE_H
#define SOMMAIRE_H

#include <QPushButton>
#include <QHBoxLayout>

class fenetreAjout;
class FenetreChargement;

class fenetreSommaire: public QWidget{
    Q_OBJECT
        QPushButton *chargementBouton; /*!< bouton permettant de lancer la fenêtre de chargement des automates */
        QPushButton *ajoutBouton; /*!< bouton permettant de lancer la fenêtre pour ajouter un automate dans le fichier XML */
        QPushButton *quitterBouton; /*!< bouton permettant de fermer la fenêtre sommaire */
        QVBoxLayout *sommaireLayout; /*!< label "sommaire" */
        fenetreAjout *ajout;

   public:
        /*!
         * \brief constructeur de la fenêtre sommaire
         */
        explicit fenetreSommaire(QWidget* parent=0);

   public slots:
        /*!
            * \brief Slot permettant d'afficher la fenêtre pour ajouter un automate
            * dans le fichier XML
            */
        void lancerFenetreAjout();

        /*!
            * \brief Slot pour fermer la fenêtre
            */
        void fin();

        /*!
            * \brief Slot permettant d'afficher la fenêtre qui simule les automates
            * du fichier XML
            */
        void lancerFenetreChargement();
};

#endif // SOMMAIRE_H

