#include <iostream>
#include "automate.h"
#include "etat.h"
#include "simulateur.h"
#include "autocellexception.h"

Simulateur::Simulateur(const Automate& a, unsigned int buffer): automate(a), depart(nullptr){
    nbMaxEtats = buffer;
    rang=0;
    etats = new Etat*[nbMaxEtats];
    for(unsigned int i=0;i<nbMaxEtats;i++){
        etats[i]=nullptr;
    }
}

Simulateur::Simulateur(const Automate &a, Etat &dep, unsigned int buffer): automate(a), depart(&dep) {
    nbMaxEtats = buffer;
    rang = 0;
    etats = new Etat*[nbMaxEtats];
    Etat* etatDepart = new Etat(dep);
    for(unsigned int i=0;i<nbMaxEtats;i++){
        etats[i]=nullptr;
    }
    etats[0] = etatDepart;
}

Simulateur::~Simulateur(){
    delete[] etats;//les etats ont deja été detruits par le destructeur d'etat, donc pas besoin de les detruire dans chaque cas du tableau etat
}

void Simulateur::setEtatDepart(const Etat &e){
    *depart = e; //On modifie la valeur de l'état de départ, pointé par depart
    reset();//On effectue un reset puisque l'état de depart a été modifié, on repart à 0
}

void Simulateur::build(unsigned int numEtats, unsigned int taille, const Automate& a) {//Prend en parametre le numero de l'état, la taille d'un etat
                                                                                     //ainsi que l'automate simulé pour pouvoir appeler le constructeur d'état
    if (numEtats >= nbMaxEtats)
        throw AutoCellException("erreur taille buffer");
    if (etats[numEtats] == nullptr)
        etats[numEtats] = new Etat(taille, a);
}

void Simulateur::reset() {//Cette methode sert à reinitialiser l'automate à son etat de depart
    if (depart==nullptr)
        throw AutoCellException("etat depart indefini");
    build(0, depart->getTaille(), automate);
    rang = 0; //On remet le rang à 0
    *etats[0] = *depart; //Au debut,  premiere case du tableau d'etat contient l'etat de depart
}

Etat& Simulateur::dernier(){//On veut recuperer l'etat le plus recent
    unsigned int r = getRangDernier();
    return *etats[r%nbMaxEtats];
}

void Simulateur::next(){
    Etat copieEtat = dernier();
    rang++;
    build(rang%nbMaxEtats, depart->getTaille(), automate);
    automate.appliquerTransition(copieEtat, *etats[rang%nbMaxEtats]);
}

void Simulateur::run(unsigned  int n){
    for(unsigned int i=0;i<n;i++){
        next();
    }
}
