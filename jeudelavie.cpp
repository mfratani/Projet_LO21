#include "automate.h"
#include "JeuDeLaVie.h"
#include "etat.h"
#include <iostream>

int JeuDeLaVie::torique(int n, const Etat& e)const{
    if(n==-1)//Si on déborde par le haut ou la gauche, on recupere la cellule d'indice taille
        return ((e.getTaille())-1);
    if(n==(e.getTaille()))//Si on déborde par le bas ou la droite, on recupere la cellule d'indice 0
        return 0;
    else
        return n;//Sinon on ne fait rien

}

//Dans le jeu de la vie, les cellules ne peuvent prendre que 2 etats différents
unsigned int JeuDeLaVie::destin(unsigned int colonne, unsigned int ligne,const Etat& e)const{//retourne vrai si les condition de survie son remplies, faux sinon
    unsigned int voisins = nbVoisins(colonne, ligne, e); //Calcul du nombre de voisin avec la methode nbVoisin
    if(e.getCellule(colonne, ligne)==1){
        if(voisins>=nbVoisinsMin && voisins<=nbVoisinsMax){
            return 1;
        }
        else{
            return 0;
        }
    }else{
        if(voisins==3){
            return 1;
        }
        else{
            return 0;
        }
    }
}

 int JeuDeLaVie::nbVoisins(unsigned int colonne, unsigned int ligne, const Etat& e)const{
     unsigned int voisins=0; //On initialise le nombre de voisin à 0
     e.getCellule(colonne, ligne); //On recupere la cellule en question
     //On ajoute 1 à voisin pour chaque voisin vivant de la cellule
     //Grace à la fonction torique, on peut acceder aux voisins de cellule en bord de matrice
     voisins +=e.getCellule(torique(colonne-1, e), torique(ligne-1, e));
     voisins +=e.getCellule(torique(colonne-1, e), torique(ligne, e));
     voisins +=e.getCellule(torique(colonne-1, e), torique(ligne+1, e));
     voisins +=e.getCellule(torique(colonne, e), torique(ligne+1, e));
     voisins +=e.getCellule(torique(colonne+1, e), torique(ligne+1, e));
     voisins +=e.getCellule(torique(colonne+1, e), torique(ligne, e));
     voisins +=e.getCellule(torique(colonne+1, e), torique(ligne-1, e));
     voisins +=e.getCellule(torique(colonne, e), torique(ligne-1, e));
     return voisins;
 }
