#ifndef FEUDEFORET
#define FEUDEFORET

class Automate;
class AutomateManager;
class Etat;

/*! \class FeuDeForet
   * \brief Classe de l'automate cellulaire feu de foret
   *
   * Les cellules peuvent être dans un des quatre états suivants : Arbre, Vide, Feu ou Cendre.
   * On peut choisir la direction du vent pour la propagation du feu
   * Hérite de la classe automate
   */
class FeuDeForet: public Automate{
    private:
        bool nord; /*!< direction du vent nord */
        bool sud; /*!< direction du vent sud */
        bool est; /*!< direction du vent est */
        bool ouest; /*!< direction du vent ouest */

        /*!
            *  \brief Constructeur
            *
            * le constructeur est privé car la gestion des automates se fait avec AutomateManager
            *  \param nom : nom de l'automate
            *   \param N,S,E,O : direction du vent
            */
        FeuDeForet(std::string nom, bool N, bool S,bool E, bool O):nord(N),sud(S), est(E), ouest(O){
            Automate::nomAutomate = nom;
            Automate::dimension = 2;
            Automate::nombreEtats = 4;
        }

        /*!
            *  \brief Destructeur
            *
            * privé car la gestion des automates 1D se fait avec AutomateManager
            */
        virtual ~FeuDeForet(){}

    public:
        friend class AutomateManager;

        /*!
            *  \brief accesseur pour le vent nord
            * \return nord
            */
        bool getVentNord()const{return nord;}

        /*!
            *  \brief accesseur pour le vent sud
            * \return sud
            */
        bool getVentSud()const{return sud;}

        /*!
            *  \brief accesseur pour le vent est
            * \return est
            */
        bool getVentEst()const{return est;}

        /*!
            *  \brief accesseur pour le vent ouest
            * \return ouest
            */
        bool getVentOuest()const{return ouest;}

        /*! \brief Renvoie le prochain état de la cellule de coordonnées[ligne][colonne]
         */
        virtual unsigned int destin(unsigned int ligne, unsigned int colonne, const Etat& e)const;

        /*! \brief méthode permettant de renvoyer le type de l'automate
         * \return FDF
         */
        std::string getType() const {return "FDF";}
};

#endif // FEUDEFORET

