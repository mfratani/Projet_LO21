#ifndef ETAT
#define ETAT

class Automate;


/*! \class Etat
     * \brief Un état représente l'état de toutes les cellules de l'automate
     * à un instant t
 */
class Etat{
    private:
        int** valeur; /*!< tableau contenant l'état, Si 1 dim, valeur[1][taille], sinon, valeur[taille][taille] */
        unsigned int taille; /*!< taille de l'automate */
        unsigned int dimension; /*!< dimension de l'automate (1 ou 2) */

    public:
        /*!
             * \brief Constructeur d'Etat
             *
             * on spécifie obligatoirement la taille et la dimension de l'automate à laquelle on accède via une réference
             * vers cet automate, afin d'éviter que l'utilisateur ne rentre une dimension qui ne correspond pas à l'automate
             *
             * \param taille : taille de l'automate
             * \param a : reference de l'automate
         */
        Etat(unsigned int taille, const Automate& a);

        /*!
            * \brief Constructeur par recopie d'un Etat
         */
        Etat(const Etat& e);

        /*!
             * \brief Modifie la valeur de l'état d'une cellule.
             * \param val : valeur à attribuer à la cellule
             * \param colonne : colonne de la cellule à modifier
             * \param ligne : ligne de la cellule à modifier
         */
        void setCellule(int val, unsigned int colonne , unsigned int ligne=0);

        /*!
             * \brief Accesseur poour récupérer la valeur d'une cellule.
             * \param colonne : colonne de la cellule à récupérer
             * \param ligne : ligne de la cellule à récupérer
         */
        int getCellule(unsigned int colonne, unsigned int ligne=0)const;

        /*!
            * \return taille
         */
        unsigned int getTaille()const {return taille;}

        /*!
            * \return dimension
         */
        unsigned int getDimension()const {return dimension;}

        /*!
            * \brief Destructeur
         */
        ~Etat();

        /*!
            * \brief surcharge de l'opérateur d'affectation
         */
        Etat& operator=(const Etat& e);
};

#endif // ETAT

