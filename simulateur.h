#ifndef SIMULATEUR
#define SIMULATEUR

class Automate;
class Etat;
class AutoCellException;

/*! \class Simulateur
 * \brief Le simulateur va stocker un nombre fixé d'état, et générer les états suivants
 */
class Simulateur{
    private:
        const Automate& automate; /*!< La reference vers l'automate que le Simulateur simule */
        Etat** etats = nullptr; /*!< Le tableau de pointeurs pointant vers les états simulés */
        unsigned int nbMaxEtats; /*!< Le nombre d'états que stocke le simulateur dans son buffer */
        unsigned int rang; /*!< Le rang du simulateur, c'est a dire le nombre d'itérations effectuées */
        Etat* depart; /*!< un pointeur vers l'état de départ */

        /*!
         * \brief Méthode privée qui alloue la mémoire pour un état à une case du buffer
         */
        void build(unsigned int cellule, unsigned int taille, const Automate& a);

        Simulateur(const Simulateur& s) = delete; //On veut empêcher la duplication de simulateur
        Simulateur& operator=(const Simulateur& s) = delete;

   public:
        /*!
             * \brief Constructeur 1 de Simulateur sans état de départ
             * \param a : l'automate que le simulateur va gérer
         */
        Simulateur(const Automate& a, unsigned int buffer = 2);

        /*!
         * \brief Constructeur 2 de Simulateur avec un état de départ
         * \param a : l'automate que le simulateur va gérer
         * \param dep : l'état de départ de l'automate
         */
        Simulateur(const Automate& a, Etat &dep, unsigned int buffer = 2);

        /*!
         *\brief destructeur
         */
        ~Simulateur();

        /*!
         * \brief Modifie l'état de départ du simulateur (et entraine un reset)
         * \param a : l'automate que le simulateur va gérer
         * \param dep : l'état de départ de l'automate
         */
        void setEtatDepart(const Etat& e);

        /*!
         * \brief applique n fois la fonction next()
         */
        void run(unsigned int n);

        /*! \brief Génère l'état suivant
         */
        void next();

        /*! \brief accesseur pour l'état de départ
         */
        Etat& getEtatDepart() const {return *depart;}

        /*! \brief Renvoie le rang du dernier état stocké dans le simulateur
         */
        unsigned int getRangDernier() const{return rang;}

        /*! \brief Retourne l'état le plus récent
         */
        Etat& dernier();

        /*! \brief Réinitialise le simulateur à l'état de départ
         */
        void reset();

        /*! \brief Renvoie une reference sur l'automate simulé
         */
        const Automate& getAutomate() const{return automate;}

        friend class Iterator;

        /*! \class Iterator
           * \brief Implémentation du design pattern iterator pour parcourir les états
           * stockés dans le simulateur
           */
        class Iterator{
            friend class Simulateur;

            private:
                    int i=0;
                    Simulateur* sim;

                    Iterator(Simulateur* s):i(s->rang),sim(s){}

            public:
                    Iterator(){}

                    bool isDone(){
                        if(sim==nullptr || (i==-1&&sim->rang<sim->nbMaxEtats) ||i == sim->rang-sim->nbMaxEtats){
                            return true;
                        }
                    }

                    /*!
                        *  \brief retourne une reference sur l'état pointé par l'iterateur
                        */
                    const Etat& current(){
                        return *(sim->etats[i%sim->nbMaxEtats]);
                    }

                    /*!
                        *  \brief incrémenter l'itérateur
                        */
                    void next(){
                        i--;
                        if(i==-1 && sim->rang>=sim->nbMaxEtats)
                            i=sim->nbMaxEtats-1;
                    }
        };
};
#endif // SIMULATEUR

