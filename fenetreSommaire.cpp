#include <QApplication>
#include <iostream>
#include "fenetreSommaire.h"
#include "fenetreAjout.h"
#include "fenetrechargement.h"
#include "automatesManager.h"

fenetreSommaire::fenetreSommaire(QWidget *parent): QWidget(parent), ajout(nullptr){
    chargementBouton = new QPushButton("Charger les automates");
    ajoutBouton = new QPushButton("Ajouter un automate");
    quitterBouton = new QPushButton("Quitter");
    sommaireLayout = new QVBoxLayout(this);
    sommaireLayout->addWidget(chargementBouton);
    sommaireLayout->addWidget(ajoutBouton);
    sommaireLayout->addWidget(quitterBouton);
    connect(ajoutBouton,SIGNAL(clicked()),this,SLOT(lancerFenetreAjout()));
    connect(quitterBouton,SIGNAL(clicked()),qApp,SLOT(quit()));
    connect(chargementBouton,SIGNAL(clicked(bool)),this,SLOT(lancerFenetreChargement()));
    chargementBouton->show();
    ajoutBouton->show();
}

void fenetreSommaire::lancerFenetreAjout(){
    fenetreAjout *a = new fenetreAjout();
    a->show();

}

void fenetreSommaire::lancerFenetreChargement(){
            FenetreChargement *c = new FenetreChargement();
            c->show();
}

void fenetreSommaire::fin(){
    AutomateManager::getAutomateManager().libererAutomateManager();
}


