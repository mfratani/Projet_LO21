/****************************************************************************
** Meta object code from reading C++ file 'fenetrechargement.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "fenetrechargement.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'fenetrechargement.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_FenetreChargement_t {
    QByteArrayData data[17];
    char stringdata0[222];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FenetreChargement_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FenetreChargement_t qt_meta_stringdata_FenetreChargement = {
    {
QT_MOC_LITERAL(0, 0, 17), // "FenetreChargement"
QT_MOC_LITERAL(1, 18, 20), // "generationEtatDepart"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 21), // "afficherInfosAutomate"
QT_MOC_LITERAL(4, 62, 3), // "nom"
QT_MOC_LITERAL(5, 66, 16), // "afficherEtatSlot"
QT_MOC_LITERAL(6, 83, 10), // "Activation"
QT_MOC_LITERAL(7, 94, 21), // "Recuperer_ancien_etat"
QT_MOC_LITERAL(8, 116, 26), // "fermetureFenetreChargement"
QT_MOC_LITERAL(9, 143, 9), // "Aleatoire"
QT_MOC_LITERAL(10, 153, 18), // "foret_complete_FDF"
QT_MOC_LITERAL(11, 172, 16), // "lancerSimulation"
QT_MOC_LITERAL(12, 189, 11), // "etatSuivant"
QT_MOC_LITERAL(13, 201, 3), // "run"
QT_MOC_LITERAL(14, 205, 5), // "pause"
QT_MOC_LITERAL(15, 211, 4), // "save"
QT_MOC_LITERAL(16, 216, 5) // "reset"

    },
    "FenetreChargement\0generationEtatDepart\0"
    "\0afficherInfosAutomate\0nom\0afficherEtatSlot\0"
    "Activation\0Recuperer_ancien_etat\0"
    "fermetureFenetreChargement\0Aleatoire\0"
    "foret_complete_FDF\0lancerSimulation\0"
    "etatSuivant\0run\0pause\0save\0reset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FenetreChargement[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x0a /* Public */,
       3,    1,   85,    2, 0x0a /* Public */,
       5,    0,   88,    2, 0x0a /* Public */,
       6,    1,   89,    2, 0x0a /* Public */,
       7,    0,   92,    2, 0x0a /* Public */,
       8,    0,   93,    2, 0x0a /* Public */,
       9,    0,   94,    2, 0x0a /* Public */,
      10,    0,   95,    2, 0x0a /* Public */,
      11,    0,   96,    2, 0x0a /* Public */,
      12,    0,   97,    2, 0x0a /* Public */,
      13,    0,   98,    2, 0x0a /* Public */,
      14,    0,   99,    2, 0x0a /* Public */,
      15,    0,  100,    2, 0x0a /* Public */,
      16,    0,  101,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void FenetreChargement::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FenetreChargement *_t = static_cast<FenetreChargement *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->generationEtatDepart(); break;
        case 1: _t->afficherInfosAutomate((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->afficherEtatSlot(); break;
        case 3: _t->Activation((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 4: _t->Recuperer_ancien_etat(); break;
        case 5: _t->fermetureFenetreChargement(); break;
        case 6: _t->Aleatoire(); break;
        case 7: _t->foret_complete_FDF(); break;
        case 8: _t->lancerSimulation(); break;
        case 9: _t->etatSuivant(); break;
        case 10: _t->run(); break;
        case 11: _t->pause(); break;
        case 12: _t->save(); break;
        case 13: _t->reset(); break;
        default: ;
        }
    }
}

const QMetaObject FenetreChargement::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_FenetreChargement.data,
      qt_meta_data_FenetreChargement,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FenetreChargement::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FenetreChargement::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FenetreChargement.stringdata0))
        return static_cast<void*>(const_cast< FenetreChargement*>(this));
    return QWidget::qt_metacast(_clname);
}

int FenetreChargement::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
