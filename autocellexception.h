#ifndef AUTOCELLEXCEPTION
#define AUTOCELLEXCEPTION

/*!
 * \file AutoCellException.h
 * \brief Gestion des exceptions
 */

#include <string>

/*! \class AutoCellException
   * \brief Classe permettant de gérer les exceptions
   */
class AutoCellException {//Classe qui va contenir les excetpions
    private:
        std::string info; /*!< Informations sur l'exception */
    public:
        /*!
            *  \brief Constructeur
            *  \param message : informations sur l'exception
            */
        AutoCellException(const std::string& message) :info(message) {}
        /*!
             *  \brief accesseur permettant de récupérer l'information sur l'exception
             */
        std::string getInfo() const { return info; }
};


#endif // AUTOCELLEXCEPTION

