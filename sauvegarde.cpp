#include "autocellexception.h"
#include "etat.h"
#include "sauvegarde.h"
#include "automate.h"

#include <QTextStream>
#include <QtXml>
#include <iostream>
#include <vector>
#include <QMessageBox>

using namespace std;

void Sauvegarde_etat(const string &fichier, const std::string& name, const Etat& e)
{
    QDomDocument document("xml");

    QFile file(QString::fromStdString(fichier));//Ouverture du fichier XML

    //Vérification de l'ouverture du fichier en lecture
    if(!file.open(QIODevice::ReadOnly)){
        throw AutoCellException("Erreur ouverture du fichier");
     }

    //Vérification de l'attribution du fichier à l'objet QDomDocument
    if(!document.setContent(&file)){
      throw AutoCellException("Erreur set content");
    }
    file.close();


    //renvoie la racine
    QDomElement automates = document.documentElement();

    //Renvoie la 1ere balise "Automate"
    QDomNode automate = automates.firstChild();

    unsigned int taille = e.getTaille();
    unsigned int nb_ligne;

    if (e.getDimension()==1)
    {
        nb_ligne = 1;
    }
    else
    {
        nb_ligne = taille;
    }

    //parcours des balises "automate" pour trouver l'automate voulu
    while(!automate.isNull())
    {
        QDomElement elm = automate.toElement();

        if (!elm.isNull())
        {
            //Si l'automate courant est celui recherché
            if (elm.attribute("Nom") == QString::fromStdString(name))
            {

                //si balise Etat déjà présente : modification
                if (elm.firstChild().toElement().tagName() == "Etat")
                {
                    elm.removeChild(elm.firstChild());

                }
                    elm.setAttribute("Taille",taille);

                    QDomElement etat = document.createElement("Etat");//création balise Etat
                    elm.appendChild(etat);//filiation de la balise Etat
                    for (unsigned int l=0;l<nb_ligne;l++)
                    {
                        QDomElement ligne = document.createElement("Ligne");//création balise Ligne

                        etat.appendChild(ligne);//filiation de la balise Ligne
                        for (unsigned int c=0;c<taille;c++)
                        {
                            QDomElement cellule = document.createElement("Cellule");//création balise Cellule

                            ligne.appendChild(cellule);//filiation de la balise Cellule
                            cellule.setAttribute("Value",e.getCellule(c,l)); //chaque cellule a un attribut "value" contenant son état

                        }
                    }
            }
        }
        automate = automate.nextSibling();
    }

    //ouverture du fichier en écriture
    QFile file2(QString::fromStdString(fichier));
    if (!file2.open(QIODevice::WriteOnly))
        throw AutoCellException("Erreur ouverture du fichier");

    //sauvegarde de l'état de l'automate dans le fichier
    QTextStream stream (&file2);
    stream << document.toString();
    file2.close();
}

Etat Chargement_dernier_etat(const std::string& fichier, const Automate &a)
{
    QDomDocument document("xml");

    QFile file(QString::fromStdString(fichier));//Ouverture du fichier XML

    //Vérification de l'ouverture du fichier en lecture
    if(!file.open(QIODevice::ReadOnly)){
        throw AutoCellException("Erreur ouverture du fichier");
     }

    //Vérification de l'attribution du fichier à l'objet QDomDocument
    if(!document.setContent(&file)){
      throw AutoCellException("Erreur set content");
    }
    file.close();


    unsigned int l = 0;
    unsigned int taille,nb_ligne;

    unsigned int automate_trouve = 0;

    vector<vector<int>> old_state;//Tableau dynamique qui va récupérer l'état de chaque cellule enregistré dans le fichier

    //variable fonctionnant comme un tableau contenant les valeurs des cellules d'une ligne
    QDomNodeList tab;

    //récupération de la racine
    QDomElement automates = document.documentElement();

    //Récupération de la 1ere balise "Automate"
    QDomNode automate = automates.firstChild();

    //parcours des balises "automate" pour trouver l'automate voulu
    while(!automate.isNull())
    {
        //Conversion du noeud en un élément
        QDomElement elm = automate.toElement();

        if (!elm.isNull())
        {
            //Si l'automate courant est celui recherché
            if (elm.attribute("Nom") == QString::fromStdString(a.getNomAutomate()))
            {

                automate_trouve = 1; //indique que l'automate recherché a été trouvé

                //Récupération de la taille (correspondant au nombre de cellules par ligne)
                taille = elm.attribute("Taille").toInt();

                //Récupération du nombre de lignes de l'état
                if (a.getDimension()==1)
                    nb_ligne = 1;
                else
                    nb_ligne = taille;

                //Agrandissement du tableau old_state pour récupérer les états de toutes les cellules enregistrées
                for (unsigned int i=0; i<nb_ligne; i++)
                    old_state.push_back(vector<int>(taille));

                //récupération de la balise Etat
                QDomElement etat = elm.firstChild().toElement();

                //Vérification de l'existence d'un état dans le fichier
                if (etat.isNull())
                {
                    throw AutoCellException("Pas d'état enregistré pour cet automate");
                }

                while(!etat.isNull())//parcours de la balise "Etat"
                {

                    QDomElement ligne = etat.firstChild().toElement();

                    //parcours de chaque ligne
                    while(!ligne.isNull())
                    {
                        //tab récupère toutes les balises "cellule" filles de la balise "ligne"
                        tab = ligne.childNodes();

                        //Récupération de l'état de chaque cellule de la ligne dans le tableau old_state
                        for (unsigned int c=0; c<tab.length();c++)
                            old_state[l][c]=tab.item(c).toElement().attribute("Value").toInt();

                        ligne = ligne.nextSibling().toElement();
                        l++;
                    }
                    etat=etat.nextSibling().toElement();
                }

            }

        }
        automate = automate.nextSibling();
    }
    if (automate_trouve == 0)
            throw AutoCellException("Aucun automate porte le nom passé en paramètre");

    //Préparation de l'état renvoyé par la fonction
    Etat e(taille,a);

    for (unsigned int k=0; k<nb_ligne; k++ )
    {
        for (unsigned int p=0; p<taille; p++)
            e.setCellule(old_state[k][p],p,k);//Attribution de l'état de chaque cellule dans le tableau "valeur"
    }

    return e;

}
