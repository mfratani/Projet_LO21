#include <iostream>

#include "autocellexception.h"
#include "automate.h"
#include "automatesManager.h"
#include "JeuDeLaVie.h"
#include "automate1d.h"
#include "automatespecifier.h"
#include "feudeforet.h"


AutomateManager::AutomateManager(){//Constructeur
    nbAutomate=0;
    nbMaxAutomate = 3;
    automates = new Automate*[nbMaxAutomate];
    for (unsigned int i = 0; i < nbMaxAutomate ; i++)
        automates[i] = nullptr;//on initialise tous les emplacements à vide
}

AutomateManager::~AutomateManager(){//Destructeur
    for(unsigned int i = 0; i < getNbMaxAutomate() ; i++){
        if(automates[i]==nullptr)
            delete automates[i];//On désalloue la mémoire si l'emplacement du tableau est occupé
    }
}

AutomateManager* AutomateManager::instance = nullptr; //Initialisation de l'instance a nullptr

AutomateManager& AutomateManager::getAutomateManager(){ //Accesseur pour l'instance de automateManager
    if (!instance)//Si il n'y a pas d'instance créee, l'instancier
        instance = new AutomateManager;
    return *instance;
}

void AutomateManager::libererAutomateManager(){//destructeur de Automate Manager
    if(instance!=nullptr){
        delete instance;
        instance = nullptr;
    }
}

void AutomateManager::viderAutomateManager(){
    for(unsigned int i=0 ; i<nbAutomate; ++i)
        delete automates[i];
    nbAutomate=0;
}

const Automate& AutomateManager::getAutomate(unsigned short int num) const{
    if(automates[num]==nullptr)
        throw AutoCellException("Erreur: automate[num]=nullptr");
    return *automates[num];
}

const Automate& AutomateManager::getAutomate(std::string nom) const {
    unsigned int i;
    while (getAutomate(i).getNomAutomate()!=nom && i<getNbAutomate()){
        i++;
    }
    if(i>= getNbAutomate())
        throw AutoCellException("Erreur depassement");
    else{
        return *automates[i];
    }
}

void AutomateManager::creerAutomate(std::string t, AutomateSpecifier s){//methode de création d'un automate

    if(getNbAutomate() == getNbMaxAutomate()) //on doit agrandir le tableau
    {
        Automate** newtab = new Automate*[getNbMaxAutomate()+10];
        for(unsigned int i =0; i<getNbAutomate(); ++i)
        {
            newtab[i]=automates[i];
        }
        Automate** old=automates;
        automates = newtab;
        nbMaxAutomate += 10;
        delete[] old;
    }
    // appel au constructeur selon le type de l'automate entré en parametre
        if(t== "JDV"){
            automates[nbAutomate] =  new JeuDeLaVie(s.getNomAutomate(), s.getNbMin(), s.getNbMax(), s.getNbVie());//Instanciation à la premiere case vide de l'automate manager
        }
        if(t=="D1"){
            if(s.getNumRegle()>= 0) //on a le numero de la regle
            {
                automates[nbAutomate] = new Automate1D(s.getNomAutomate(), s.getNumRegle());
            }
            else if (s.getNumRegle() < 0){ //on a le numero binaire de la regle
                automates[nbAutomate] = new Automate1D(s.getNomAutomate(),s.getStrRegle());
            }
        }
        if(t=="FDF"){
            automates[nbAutomate] = new FeuDeForet(s.getNomAutomate(), s.getVentNord(), s.getVentSud(), s.getVentEst(), s.getVentOuest());
        }

        //######Inserer code pour créer un nouveau type d'automate

    nbAutomate++;//incrementation du compteur d'automate.
}




