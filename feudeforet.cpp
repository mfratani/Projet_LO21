#include "automate.h"
#include "etat.h"
#include "feudeforet.h"
#include <iostream>

unsigned int FeuDeForet::destin(unsigned int colonne, unsigned int ligne, const Etat& e)const {
    if(e.getCellule(colonne, ligne)==0){ //Si la cellule est vide, elle reste vide
        return 0;
    }
    if(e.getCellule(colonne, ligne)==1){//Si la cellule est un arbre
        if(nord){ //Si le vent Nord est activé
            if((e.getCellule(colonne-1, ligne+1)==3 && ligne+1<e.getTaille() && colonne-1<e.getTaille())||(e.getCellule(colonne, ligne+1)==3  && ligne+1<e.getTaille())||(e.getCellule(colonne+1, ligne+1)==3 && colonne+1<e.getTaille() && ligne+1<e.getTaille())){
                return 3;
            }
        }
        if(sud){ //Si le vent Sud est activé
            if((e.getCellule(colonne-1, ligne-1)==3 && colonne-1<e.getTaille() && ligne-1<e.getTaille())||(e.getCellule(colonne, ligne-1)==3 && ligne-1<e.getTaille())||(e.getCellule(colonne+1, ligne-1)==3 && colonne+1<e.getTaille() && ligne-1<e.getTaille())){
                return 3;
            }
        }
        if(est){ //Si le vent Ouest est activé)
            if((e.getCellule(colonne-1, ligne-1)==3 &&colonne-1<e.getTaille() && ligne-1<e.getTaille())||(e.getCellule(colonne-1, ligne)==3 && colonne-1<e.getTaille())||(e.getCellule(colonne-1,ligne+1)==3&&ligne+1<e.getTaille() && colonne-1<e.getTaille())){
                return 3;
            }
        }
        if(ouest){ //Si le vent Ouest est activé
            if((e.getCellule(colonne+1, ligne-1)==3 && colonne+1<e.getTaille() && ligne-1<e.getTaille())||(e.getCellule(colonne+1, ligne)==3 && colonne+1<e.getTaille())||e.getCellule(colonne+1, ligne+1)==3 && colonne+1<e.getTaille()&&ligne+1<e.getTaille()){
                return 3;
            }
        }
        return 1;
    }
    if(e.getCellule(colonne, ligne)==2){//Si la cellule est une cellule de cendre, elle devient vide
        return 0;
    }
    if(e.getCellule(colonne, ligne)==3){//Si la cellule est en feu, elle devient une cendre
        return  2;
    }
}


