#ifndef FENETREAJOUT_H
#define FENETREAJOUT_H

#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QTabWidget>
#include <QLineEdit>
#include <QWidget>
#include <QSpinBox>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>

class fenetreAjout: public QWidget{
    Q_OBJECT
        QTabWidget *onglets; /*!< onglets permettant de choisir entre les différents types d'automates*/
        QPushButton *validerBouton; /*!< bouton permettant l'insertion des informations saisies dans le fichier XML */
        QPushButton *annulerBouton; /*!< bouton permettant de fermer la fenêtre d'ajout */

    //Page dédiée au jeu de la vie------------------------------------------------
        QWidget *page1; /*!< page pour ajouter un jeu de la vie */

        QLabel *label1Page1;
        QLabel *label2Page1;
        QLabel *label3Page1;
        QLabel *label4Page1;
        QLabel *label5Page1;

        QLineEdit *lineEdit1Page1; /*!< lineedit permettant de rentrer le nom de l'automate */
        QLineEdit *lineEdit2Page1; /*!< lineedit permettant de rentrer le nombre de voisins minimum pour que la cellule survive */
        QLineEdit *lineEdit3Page1; /*!< lineedit permettant de rentrer le nombre de voisins maximum pour que la cellule survive */
        QLineEdit *lineEdit4Page1; /*!< lineedit permettant de rentrer le nombre de voisins pour que la cellule renaisse */

        QHBoxLayout *Hlayout1Page1;
        QHBoxLayout *Hlayout2Page1;
        QHBoxLayout *Hlayout3Page1;
        QHBoxLayout *Hlayout4Page1;

        QVBoxLayout *VlayoutPage1;

   //Page dédiée à l'automate à 1 dimension -----------------------------------
        QWidget *page2; /*!< page pour ajouter un automate 1D */
        QLabel *label1Page2;
        QLabel *label3Page2;
        QLabel *label4Page2;
        QLineEdit *lineEdit1Page2; /*!< lineedit permettant de rentrer le nom de l'automate */
        QHBoxLayout* Hlayout1Page2;
        QVBoxLayout* VlayoutPage2;
        QSpinBox* QSpinBoxNumPage2; /*!< spinbox permettant de rentrer le numero de la règle */
        QHBoxLayout* Hlayout2Page2;
        QIntValidator* zeroOneValidator;
        QLabel* numeroBitl[8];
        QLineEdit* numeroBit[8]; /*!< lineedit permettant de rentrer le numero de la règle en bit (8) */
        QVBoxLayout* bitc[8];

    //Page dédiée à l'automate FDF --------------------------------------------
        QWidget *page3; /*!< page pour ajouter un feu de forêt */
        QLabel *label1Page3;

        QLineEdit *lineEdit1Page3; /*!< lineedit permettant de rentrer le nom de l'automate */

        QVBoxLayout *Vlayout1Page3;
        QVBoxLayout *VLayout2Page3;

        QHBoxLayout *Hlayout1Page3;
        QHBoxLayout *Hlayout2Page3;

        QGroupBox *groupBox1Page3; /*!< groupbox permettant de stocker les checkbox */

        QCheckBox *ventNordRadio; /*!< checkbox permettant d'ajouter un vent nord */
        QCheckBox *ventSudRadio; /*!< checkbox permettant d'ajouter un vent sud */
        QCheckBox *ventEstRadio; /*!< checkbox permettant d'ajouter un vent est */
        QCheckBox *ventOuestRadio; /*!< checkbox permettant d'ajouter un vent ouest */

    public:
        explicit fenetreAjout(QWidget *parent = 0);

    public slots:
        /*!
            *  \brief Slot permettant d'ajouter un jeu de la vie dans le fichier XML
         */
        void ajoutJeuDeLaVie();

        /*!
            *  \brief Slot permettant d'ajouter un automate 1D dans le fichier XML
        */
        void ajoutAutomate1D();

        /*!
            *  \brief Slot permettant d'ajouter un feu de forêt dans le fichier XML
         */
        void ajoutFeuDeForet();

        /*!
            * \brief Slot utilisé lors de l'ajout d'un automate 1D pour synchroniser les modifications
            * du numéro de la règle en décimal vers l'affichage du numéro de la règle en bit.
         */
        void synchronizeNumToNumBit(int i);

        /*!
            *  \brief Slot utilisé lors de l'ajout d'un automate 1D pour synchroniser les modifications
            * du numéro de la règle en bit vers l'affichage du numéro de la règle en décimal.
        */
        void synchronizeNumBitToNum(const QString& s);
};


#endif // FENETREAJOUT_H

