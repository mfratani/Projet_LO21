#ifndef AUTOMATE
#define AUTOMATE

#include <string>

#include "automatesManager.h"

class Etat;

/*! \class Automate
   * \brief Classe abstraite des automates.
   * Héritée par tous les types d'automates (jeu de la vie, automate 1D et feu de forêt)
   */
class Automate{
    protected:
        unsigned int dimension; /*!< La dimension de l'automate : 1 ou 2  */
        unsigned int nombreEtats; /*!< Le nombre d'états possibles pour une cellule */
        std::string nomAutomate; /*!< Le nom de l'automate permettant de l'identifier */

    public:
        friend class AutomateManager;

        /*!
            *  \brief appliquerTransition : méthode concrete
            *
            * qui va appliquer la transition d'un état de départ vers un état d'arrivée en fonction des règles definies.
            * Cette fonction doit être redefinie pour chaque type d'automates car ceux-ci ont des formats de règles différents
            * en appelant la méthode virtuelle pure destin
            *
            *  \param dep : état de départ
            *   \param arr : état d'arrivée
            */
        void appliquerTransition(const Etat& dep, Etat& arr)const;

        /*!
            *  \brief destin : méthode virtuelle pure
            *
            * renvoie la valeur que doit prendre la cellule dont les coordonnée sont passé en parametre
            *
            *
            * \param colonne: la colonne de la cellule
            *  \param ligne: la ligne de la cellule (si dimension = 2)
            *   \param e : état concerné
            */
        virtual unsigned int destin( unsigned int colonne, unsigned int ligne, const Etat& e)const = 0;

        /*!
            *  \brief accesseur pour la dimension de l'automate
            *   \return dimension
            */
        unsigned int getDimension()const {return dimension;}

        /*!
            *  \brief accesseur pour le nombre d'états possibles d'une cellule
            *   \return nombreEtats
            */
        unsigned int getNbEtats()const{return nombreEtats;}

        /*!
            *  \brief accesseur pour le nom de l'automate
            *   \return nomAutomate
            */
        std::string getNomAutomate()const{return nomAutomate;}

        /*! \brief methode virtuelle pure pour renvoyer le type de l'automate
         */
        virtual std::string getType() const =0;

        /*!
            *  \brief destructeur virtuel
            *
            * pour pouvoir utiliser le destructeur d'automate sur des classes d'automates concrètes,
            * il faudra donc le redéfinir dans chacune des sous-classes d'automate
            */
        virtual ~Automate(){}
};

#endif // AUTOMATE

