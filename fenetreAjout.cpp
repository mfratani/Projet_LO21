#include <QApplication>
#include <iostream>
#include <QMessageBox>
#include <QtXml>
#include <QDomDocument>

#include "autocellexception.h"
#include "fenetreAjout.h"
#include "automate1d.h"

fenetreAjout::fenetreAjout(QWidget *parent): QWidget(parent){
    onglets = new QTabWidget(this);
    onglets->setGeometry(30, 20, 550, 300);

 //Page1 pour le JDV---------------------------------------------
    page1 = new QWidget;
    onglets->addTab(page1, "Jeu de la vie");
    label1Page1 = new QLabel("Creation d'un jeu de la vie");
    label2Page1 = new QLabel("Entrez le nombre de voisin minimum pour que la cellule survive");
    label3Page1 = new QLabel("Entrez le nombre de voisin maximum pour que la cellule survive");
    label4Page1 = new QLabel("Entrez le nom de votre automate");
    label5Page1 = new QLabel("Entrez le nombre de voisins pour qu'un cellule revienne à la vie");
    lineEdit1Page1 = new QLineEdit();
    lineEdit2Page1 = new QLineEdit();
    lineEdit3Page1 = new QLineEdit();
    lineEdit4Page1 = new QLineEdit();
    Hlayout1Page1 = new QHBoxLayout();
    Hlayout2Page1 = new QHBoxLayout();
    Hlayout3Page1 = new QHBoxLayout();
    Hlayout4Page1 = new QHBoxLayout();
    Hlayout1Page1->addWidget(label2Page1);
    Hlayout1Page1->addWidget(lineEdit1Page1);
    Hlayout2Page1->addWidget(label3Page1);
    Hlayout2Page1->addWidget(lineEdit2Page1);
    Hlayout3Page1->addWidget(label4Page1);
    Hlayout3Page1->addWidget(lineEdit3Page1);
    Hlayout4Page1->addWidget(label5Page1);
    Hlayout4Page1->addWidget(lineEdit4Page1);
    VlayoutPage1 = new QVBoxLayout;
    VlayoutPage1->addWidget(label1Page1);
    VlayoutPage1->addLayout(Hlayout3Page1);
    VlayoutPage1->addLayout(Hlayout1Page1);
    VlayoutPage1->addLayout(Hlayout2Page1);
    VlayoutPage1->addLayout(Hlayout4Page1);
    validerBouton = new QPushButton("Valider");
    VlayoutPage1->addWidget(validerBouton);
    connect(validerBouton,SIGNAL(clicked()),this ,SLOT(ajoutJeuDeLaVie()));
    annulerBouton = new QPushButton("Annuler");
    VlayoutPage1->addWidget(annulerBouton);
    connect(annulerBouton, SIGNAL(clicked(bool)), this, SLOT(close()));
    page1->setLayout(VlayoutPage1);

 //Page 2 pour le 1D-----------------------------------------------------------------------------
    page2 = new QWidget;
    onglets->addTab(page2,"Automate 1D");
    label1Page2 = new QLabel("Creation d'un automate à une dimension");
     //entrer string numero de regle ou int numero de regle
    QSpinBoxNumPage2 = new QSpinBox(this);
    QSpinBoxNumPage2->setRange(0, 255);
    QSpinBoxNumPage2->setValue(0);
    label4Page2 = new QLabel("Entrez le nom de votre automate");
    label3Page2 = new QLabel("Entrez la regle de votre automate");
    lineEdit1Page2 = new QLineEdit();//pour récupérer le nom de l'automate
    Hlayout1Page2 = new QHBoxLayout(); //pour aligner nom automate
    Hlayout1Page2->addWidget(label4Page2);
    Hlayout1Page2->addWidget(lineEdit1Page2);
    Hlayout2Page2 = new QHBoxLayout;
    VlayoutPage2 = new QVBoxLayout;
    VlayoutPage2->addWidget(label1Page2);
    VlayoutPage2->addLayout(Hlayout1Page2);
    VlayoutPage2->addWidget(label3Page2);
    VlayoutPage2->addWidget(QSpinBoxNumPage2);
    VlayoutPage2->addLayout(Hlayout2Page2);

    zeroOneValidator = new QIntValidator(this);
    zeroOneValidator->setRange(0, 1);
    numeroBitl[0] = new QLabel("111");
    numeroBitl[1] = new QLabel("110");
    numeroBitl[2] = new QLabel("101");
    numeroBitl[3] = new QLabel("100");
    numeroBitl[4] = new QLabel("011");
    numeroBitl[5] = new QLabel("010");
    numeroBitl[6] = new QLabel("001");
    numeroBitl[7] = new QLabel("000");
    for(unsigned int counter = 0; counter < 8; ++counter) {
        numeroBit[counter] = new QLineEdit(this);
        numeroBit[counter]->setFixedWidth(20); //pixels
        numeroBit[counter]->setMaxLength(1); //caractères
        numeroBit[counter]->setText("0");
        numeroBit[counter]->setValidator(zeroOneValidator);
        bitc[counter] = new QVBoxLayout;
        bitc[counter]->addWidget(numeroBitl[counter]);
        bitc[counter]->addWidget(numeroBit[counter]);
        Hlayout2Page2->addLayout(bitc[counter]);
        connect(numeroBit[counter], SIGNAL(textChanged(QString)), this, SLOT(synchronizeNumBitToNum(QString)));
    }
    connect(QSpinBoxNumPage2, SIGNAL(valueChanged(int)), this, SLOT(synchronizeNumToNumBit(int)));
    validerBouton = new QPushButton("Valider");
    VlayoutPage2->addWidget(validerBouton);
    connect(validerBouton,SIGNAL(clicked()),this ,SLOT(ajoutAutomate1D()));
    annulerBouton = new QPushButton("Annuler");
    VlayoutPage2->addWidget(annulerBouton);
    connect(annulerBouton, SIGNAL(clicked(bool)), this, SLOT(close()));
    page2->setLayout(VlayoutPage2);
    onglets->addTab(page2, "Automate 1D");

  //Page 3 pour le FDF -------------------------------------------------------
    page3 = new QWidget();
    label1Page3 = new QLabel("Entrez le nom de l'automate");
    lineEdit1Page3 = new QLineEdit();
    Hlayout1Page3 = new QHBoxLayout();
    Hlayout2Page3 = new QHBoxLayout();
    Vlayout1Page3 = new QVBoxLayout();
    VLayout2Page3 = new QVBoxLayout();
    groupBox1Page3 = new QGroupBox("Les vents");
    ventNordRadio = new QCheckBox("Nord");
    ventSudRadio = new QCheckBox("Sud");
    ventEstRadio = new QCheckBox("Est");
    ventOuestRadio = new QCheckBox("Ouest");
    Hlayout1Page3->addWidget(label1Page3);
    Hlayout1Page3->addWidget(lineEdit1Page3);
    VLayout2Page3->addWidget(ventNordRadio);
    VLayout2Page3->addWidget(ventSudRadio);
    VLayout2Page3->addWidget(ventEstRadio);
    VLayout2Page3->addWidget(ventOuestRadio);
    groupBox1Page3->setLayout(VLayout2Page3);
    Vlayout1Page3->addLayout(Hlayout1Page3);
    Vlayout1Page3->addWidget(groupBox1Page3);
    validerBouton = new QPushButton("Valider");
    Vlayout1Page3->addWidget(validerBouton);
    connect(validerBouton,SIGNAL(clicked()),this ,SLOT(ajoutFeuDeForet()));
    annulerBouton = new QPushButton("Annuler");
    Vlayout1Page3->addWidget(annulerBouton);
    connect(annulerBouton, SIGNAL(clicked(bool)), this, SLOT(close()));
    page3->setLayout(Vlayout1Page3);
    onglets->addTab(page3, "Feu De Foret");

//###################Inserer code pour ajouter un nouveau type d'automate###########################################################
}

void fenetreAjout::ajoutJeuDeLaVie(){
    //test sur les attributs rentrés :
        if(lineEdit3Page1->text() == "")
            QMessageBox::warning(this, "Erreur", "rentrez un nom pour l'automate");
        else if(lineEdit1Page1->text() == "" || lineEdit2Page1->text() == "")
            QMessageBox::warning(this, "Erreur", "rentrez un nombre de voisins");
        else if(lineEdit1Page1->text().toInt() > lineEdit2Page1->text().toInt()) //nbvoisins min > nbvoisins max
            QMessageBox::warning(this, "Erreur","Attention, le nombre de voisins minimum ne doit pas être plus grand que le nombre de voisins maximum");
        else if(lineEdit4Page1->text() == "")
            QMessageBox::warning(this, "Erreur", "rentrez un nombre de voisins pour que la cellule revienne à la vie");
        else //si tous les attributs sont bien rentrés
        {
            QDomDocument dom("xml");

            QFile doc_xml("automates.xml");

            if(!doc_xml.open(QIODevice::ReadOnly)){
                QMessageBox::warning(this, "Erreur", "Erreur ouverture en lecture du fichier");
                doc_xml.close();
            }
            if(!dom.setContent(&doc_xml)){
                QMessageBox::warning(this, "Erreur", "Erreur setcontent du fichier");
                doc_xml.close();
            }
            doc_xml.close();
            QDomElement docElem = dom.documentElement();
            QDomElement automate = dom.createElement("Automate");
            automate.setAttribute("Nom", lineEdit3Page1->text());
            automate.setAttribute("Type", "JDV");
            automate.setAttribute("nbVoisinsMin", lineEdit1Page1->text());
            automate.setAttribute("nbVoisinsMax", lineEdit2Page1->text());
            automate.setAttribute("nbVoisinsVie", lineEdit4Page1->text());
            docElem.appendChild(automate);


            QFile file("automates.xml");

            if(!file.open(QIODevice::WriteOnly | QIODevice::Text)){
                QMessageBox::warning(this, "Erreur", "Erreur lors de l'ouverture du fichier");
                file.close();
            }
            else{
                QTextStream stream(&file);
                stream << dom.toString();
                file.close();
                QMessageBox::information(this, "Confirmation", "Votre automate a bien été enregistré");
            }
        }
}

void fenetreAjout::ajoutAutomate1D(){
        if(lineEdit1Page2->text() == "")
            QMessageBox::warning(this, "Erreur", "rentrez un nom pour l'automate");
        else
        {
            QDomDocument dom("xml");

            QFile doc_xml("automates.xml");


            if(!doc_xml.open(QIODevice::ReadOnly)){
                QMessageBox::warning(this, "Erreur", "Erreur ouverture en lecture du fichier");
                doc_xml.close();
            }
            if(!dom.setContent(&doc_xml)){
                QMessageBox::warning(this, "Erreur", "Erreur setcontent du fichier");
                doc_xml.close();
            }
            doc_xml.close();
            QDomElement docElem = dom.documentElement();
            QDomElement automate = dom.createElement("Automate");
            automate.setAttribute("Nom", lineEdit1Page2->text());
            automate.setAttribute("Type", "D1");
            automate.setAttribute("NumRegle", QSpinBoxNumPage2->value());
            automate.setAttribute("NumBitRegle", QString::fromStdString(NumToNumBit(QSpinBoxNumPage2->value())));
            docElem.appendChild(automate);


            QFile file("automates.xml");

            if(!file.open(QIODevice::WriteOnly | QIODevice::Text)){
                QMessageBox::warning(this, "Erreur", "Erreur lors de l'ouverture du fichier");
                file.close();
            }
            else{
                QTextStream stream(&file);
                stream << dom.toString();
                file.close();
                QMessageBox::information(this, "Confirmation", "Votre automate a bien été enregistré");
            }
        }
}

void fenetreAjout::ajoutFeuDeForet(){
    if(lineEdit1Page3->text() == "")
        QMessageBox::warning(this, "Erreur", "rentrez un nom pour l'automate");
    else
    {
        QDomDocument dom("xml");
        QFile doc_xml("automates.xml");

        if(!doc_xml.open(QIODevice::ReadOnly)){
            QMessageBox::warning(this, "Erreur", "Erreur ouverture en lecture du fichier");
            doc_xml.close();
        }
        if(!dom.setContent(&doc_xml)){
            QMessageBox::warning(this, "Erreur", "Erreur setcontent du fichier");
            doc_xml.close();
        }
        doc_xml.close();
        QDomElement docElem = dom.documentElement();
        QDomElement automate = dom.createElement("Automate");
        automate.setAttribute("Nom", lineEdit1Page3->text());
        automate.setAttribute("Type", "FDF");
        if(ventNordRadio->isChecked())
            automate.setAttribute("ventNord", "1");
        else
            automate.setAttribute("ventNord", "0");
        if(ventSudRadio->isChecked())
            automate.setAttribute("ventSud", "1");
        else
            automate.setAttribute("ventSud", "0");
        if(ventEstRadio->isChecked())
            automate.setAttribute("ventEst", "1");
        else
            automate.setAttribute("ventEst", "0");
        if(ventOuestRadio->isChecked())
            automate.setAttribute("ventOuest", "1");
        else
            automate.setAttribute("ventOuest", "0");
        docElem.appendChild(automate);


        QFile file("automates.xml");


        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)){
            QMessageBox::warning(this, "Erreur", "Erreur lors de l'ouverture du fichier");
            file.close();
        }
        else{
            QTextStream stream(&file);
            stream << dom.toString();
            file.close();
            QMessageBox::information(this, "Confirmation", "Votre automate a bien été enregistré. Revenez au menu pour pouvoir lancer une simulation.");
        }
      }
}

//###################Inserer code pour ajouter un nouveau type d'automate###########################################################

void fenetreAjout::synchronizeNumToNumBit(int i) {
    std::string numBit = NumToNumBit(i);
    for(unsigned int counter = 0; counter < 8; ++counter) {
        numeroBit[counter]->setText(QString(numBit[counter]));
    }
}

void fenetreAjout::synchronizeNumBitToNum(const QString& s) {
    if (s == "") {
        return;
    }
    std::string numBit = "";
    for(unsigned int counter = 0; counter < 8; ++counter) {
        numBit += numeroBit[counter]->text().toStdString();
    }
    QSpinBoxNumPage2->setValue(NumBitToNum(numBit));
}
