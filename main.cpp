#include <iostream>
#include "automatesManager.h"
#include "automatespecifier.h"
#include "automate.h"
#include "JeuDeLaVie.h"
#include "etat.h"
#include "autocellexception.h"
#include "simulateur.h"
#include "fenetreSommaire.h"
#include "fenetreAjout.h"
#include "fenetrechargement.h"
#include <QtXml>
#include <QApplication>

using namespace std;

int main(int argc, char* argv[]) {
    try{
        QApplication app(argc, argv);
        fenetreSommaire s;
        s.show();
        return app.exec();
     }
    catch (AutoCellException& e){
            cout<<e.getInfo()<<endl;
     }
     return 0;
}







