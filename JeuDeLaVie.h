#ifndef JEUDELAVIE
#define JEUDELAVIE

class Automate;
class AutomateManager;


/*! \class JeuDeLaVie
 * \brief Classe de l'automate du jeu de la vie
 * hérite de la classe automate
 */
class JeuDeLaVie: public Automate{
    private:
        //Les regles d'un jeu de la vie sont caractérisées par un de nombre voisin min et max pour induire l'état d'une cellule
        unsigned int nbVoisinsMin; /*!< nombre de voisins minimum pour que la cellule survive */
        unsigned int nbVoisinsMax; /*!< nombre de voisins maximum pour que la cellule survive */
        unsigned int nbVoisinsVie; /*!< nombre de voisins pour qu'une cellule revienne à la vie */

        /*!
            *  \brief Destructeur
            * privé car la gestion des automates se fait avec AutomateManager
            */
        virtual ~JeuDeLaVie(){}

        /*!
            *  \brief Constructeur
            *
            * privé car la gestion des automates se fait avec AutomateManager
            * \param nom : nom de l'automate
            * \param nbMin : nombre de voisins minimum pour que la cellule survive
            * \param nbMax : nombre de voisins maximum pour que la cellule survive
            * \param nbMax : nombre de voisins pour qu'une cellule revienne à la vie
            */
        JeuDeLaVie(std::string nom,unsigned int nbMin, unsigned int nbMax, unsigned int nbVie):nbVoisinsMin(nbMin), nbVoisinsMax(nbMax), nbVoisinsVie(nbVie){
            Automate::nomAutomate = nom;
            Automate::dimension=2; //Un jeu de la vie a forcément 2 dimensions
            Automate::nombreEtats = 2;//Un jeu de la vie admet 2 etats pour ses cellules
        }

    public:
        friend class AutomateManager;

        //les accesseurs en lecture

        /*! \brief accesseur pour le nombre de voisins minimum necessaires pour que la cellule survive
         * \return nbVoisinsMin
         */
        virtual unsigned int getNbVoisinsMin()const{return nbVoisinsMin;}

        /*! \brief accesseur pour le nombre de voisins maximum necessaires pour que la cellule survive
         * \return nbVoisinsMax
         */
        virtual unsigned int getNbVoisinsMax()const{return nbVoisinsMax;}

        /*! \brief accesseur pour le nombre de voisins necessaire pour que la cellule revienne à la vie
         * \return nbVoisinsVie
         */
        virtual unsigned int getNbVoisinsVie()const{return nbVoisinsVie;}

        /*! \brief méthode permettant de renvoyer le type JDV de l'automate
         * \return JDV
         */
        std::string getType() const {return "JDV";}

        /*! \brief Methode auxilliare pour accéder au valeur des voisins d'une cellule en bout de ligne/colonne
         * (la matrice est considérée circulaire)
         */
        int torique( int n, const Etat& e)const;

        /*! \brief calcule le nombre de voisins d'une cellule de l'état et de coordonées[ligne][colonne]
         */
        int nbVoisins(unsigned int ligne, unsigned int colonne, const Etat& e)const;

        /*! \brief Renvoie le prochain état de la cellule de coordonnées[ligne][colonne]
         */
        virtual unsigned int destin(unsigned int ligne, unsigned int colonne, const Etat& e)const;
};

#endif // JEUDELAVIE
