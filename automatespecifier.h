#ifndef AUTOMATESPECIFIER
#define AUTOMATESPECIFIER
#include <string>

/*! \class AutomateSpecifier
   *  \brief Classe qui va contenir tous les paramètres d'initialisation
   * pour n'importe quels types d'automates
   */
class AutomateSpecifier{
    private:
        std::string nomAutomate; /*!< nom de l'automate*/
        //JDV
        unsigned int nbMin; /*!< 1 des paramètres d'un jeu de la vie (le nombre de voisins minimum)*/
        unsigned int nbMax; /*!< 1 des paramètres d'un jeu de la vie (le nombre de voisins maximum)*/
        unsigned int nbVie; /*!< 1 des paramètres d'un jeu de la vie (le nombre de voisins pour qu'une cellule revienne à la vie)*/

        //automate1D
        std::string strRegle;/*!< 1 des paramètres d'un automate à une dimension (le numéro de la règle en bit) */
        unsigned int numRegle; /*!< 1 des paramètres d'un automate à une dimension (le numéro de la règle en décimal) */

        //Les parametres du FDF
        bool ventNord;  /*!< 1 des paramètres d'un automate feu de forêt (présence d'un vent nord ou non) */
        bool ventSud; /*!< 1 des paramètres d'un automate feu de forêt (présence d'un vent sud ou non) */
        bool ventEst; /*!< 1 des paramètres d'un automate feu de forêt (présence d'un vent est ou non) */
        bool ventOuest; /*!< 1 des paramètres d'un automate feu de forêt (présence d'un vent ouest ou non) */

        //##################Pour ajouter un nouveau type d'automate, il faut ajouter les attributs qui definissent les regles de transition ici, ainsi que les accesseurs en public

    public:
        //Les accesseurs en lecture
        std::string getNomAutomate()const {return nomAutomate;}
        unsigned int getNbMin()const {return nbMin;}
        unsigned int getNbMax()const {return nbMax;}
        unsigned int getNbVie()const {return nbVie;}
        unsigned int getNumRegle() const {return numRegle;}
        std::string getStrRegle()const {return strRegle;}
        bool getVentNord(){return ventNord;}
        bool getVentSud(){return ventSud;}
        bool getVentEst(){return ventEst;}
        bool getVentOuest(){return ventOuest;}

        /*!
            *  \brief Le constructeur, qui contient tous les paramètres
            *
            * lors de la construction d'un automate,
            * seuls les parametres requis seront utilisés par le constructeur du dit automate
            */
        AutomateSpecifier(std:: string nom, unsigned int Min, unsigned int Max, unsigned int Vie,std::string strRegleJdv, unsigned int num, bool N, bool S,bool E, bool O);
};

#endif // AUTOMATESPECIFIER

