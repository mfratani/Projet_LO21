#include "automate.h"
#include "etat.h"

void Automate::appliquerTransition(const Etat &dep, Etat &arr) const{
    unsigned int colonne = dep.getTaille();
    unsigned int hauteur = 1;

    if(dimension == 2){
        hauteur = colonne;
    }

   //On parcourt chaque cellule de l'état de depart
   for(unsigned int ligne=0;ligne<hauteur;ligne++){
        for(unsigned int colonne=0;colonne<dep.getTaille();colonne++){
            arr.setCellule(destin(colonne, ligne, dep), colonne, ligne); //On met à jour chaque cellule de l'état d'arrivée en utilisant destin et setCellule
        }
    }
}
