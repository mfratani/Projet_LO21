#ifndef CHARGEMENTAUTOMATES
#define CHARGEMENTAUTOMATES

#include <QtXml>

class AutomatesManager;

/*!
 * \brief Fonction qui charge les automates du fichier XML str dans l'Automate Manager
 * \param str : nom du fichier XML
 */
int chargementAutomates(const QString str);

#endif // CHARGEMENTAUTOMATES

