#ifndef AUTOMATEMANAGER
#define AUTOMATEMANAGER

class AutomateSpecifier;
class Automate;


/*! \class AutomateManager
   * \brief Classe singleton qui stocke et gère le cycle de vie de tous les automates
   *
   * quelque soit leur type
   */
class AutomateManager{
    private :
        Automate** automates; /*!< Tableau dans lequel sont stockés les automates */
        unsigned int nbAutomate; /*!< Le nombre d'automates actuellement stockés par l'automateManager */
        unsigned int nbMaxAutomate; /*!< Le nombre maximum d'automates que l'on peut stocker */
        static AutomateManager* instance; /*!< Pointeur static vers l'unique instance. Il est static donc préexiste déjà en mémoire avant l'execution */

        /*!
            * \brief Constructeur privé
            *
            * pour éviter que l'utilisateur ne crée plusieurs instances d'automateManager
            * (On est dans un singleton)
            */
        AutomateManager();

        /*!
            * \brief Destructeur privé
            *
            * car c'est un singleton
            * la destruction est gérée par une méthode static
            */
        ~AutomateManager();

        AutomateManager(const AutomateManager& a)=delete; //On veut également éviter la definition du constructeur par recopie et la surcharge de l'operateur d'affectation
        AutomateManager& operator= (const AutomateManager& a)=delete;

    public :
        /*!
            * \brief Méthode static qui permettra d'instancier l'automateManager.
            *
            * Elle est static, on peut donc l'appeler même si aucune instance d'automateManager n'existe
            */
        static AutomateManager& getAutomateManager();

        /*!
            * \brief Méthode static pour libérer (et donc supprimer)
            * l'instance unique de l'AutomateManager
            */
        static void libererAutomateManager();

        /*!
            * \brief Méthode permettant de détruire tous les automates
            * contenues dans l'AutomateManager
            */
        void viderAutomateManager();

        /*!
            * \brief Méthode permettant d'accéder à l'automate stocké dans automates[num]
            *
            * \param num : indice de la case "num" du tableau contenant les automates
            */
        const Automate& getAutomate(unsigned short int num) const;

        /*!
            * \brief Méthode permettant d'accéder à l'automate stocké dans automates[]
            * grâce à son nom
            *
            * \param nom : nom de l'automate que l'on souhaite récupérer
            */
        const Automate& getAutomate(std::string nom) const;

        /*!
            * \brief Méthode pour créer un automate de type t grâce à un automateSpecifier
            *
            * \param t : type de l'automate à créer
            * \param s : AutomateSpecifier contenant les informations necessaires pour créer l'automate en fonction du type
            */
        void creerAutomate(std::string t, AutomateSpecifier s);

        /*!
            * \brief accesseur en lecture de nbAutomate
            *
            * \return nbAutomate
            */
        unsigned int getNbAutomate()const{return nbAutomate;}

        /*!
            * \brief accesseur en lecture de nbMaxAutomate
            *
            * \return nbMaxAutomate
            */
        unsigned int getNbMaxAutomate() const {return nbMaxAutomate;}

        friend class IteratorManager;

        /*! \class IteratorManager
           * \brief Implémentation du design pattern iterator pour parcourir les automates
           * stockés dans l'AutomateManager
           */
        class IteratorManager{
            private:
                friend class AutomateManager;
                AutomateManager &manager;
                unsigned int i=0;
                IteratorManager(): manager(AutomateManager::getAutomateManager()){}

            public:
                bool isDone(){
                    if(i>=manager.getNbAutomate()){
                        return true;
                    }
                    else{
                        return false;
                    }
                }

                /*!
                    *  \brief retourne une réference sur l'automate pointé par l'iterateur
                    */
                Automate& current(){
                    return *manager.automates[i];
                }

                /*!
                    *  \brief incrémenter l'itérateur
                    */
                void next(){
                    if(!isDone()){
                        i++;
                    }

                }

                /*!
                    *  \brief remettre à l'indice 0 l'itérateur
                    */
                void reset(){
                    i=0;
                }
        };

        /*!
            * \brief accesseur pour récupérer l'iterateur
            *
            * \return IteratorManager()
            */
        IteratorManager getIteratorManager(){return IteratorManager();}
};

#endif // AUTOMATEMANAGER

