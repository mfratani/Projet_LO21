#include "automate.h"
#include "automate1d.h"
#include "etat.h"
#include "autocellexception.h"
#include <iostream>

short unsigned int NumBitToNum(const std::string& num) {
    if (num.size() != 8) throw AutoCellException("Numero d’automate indefini");
    int puissance = 1;
    short unsigned int numero = 0;
    for (int i = 7; i >= 0; i--)
    {
        if (num[i] == '1') numero += puissance;
        else if (num[i] != '0') throw AutoCellException("Numero d’automate indefini");
        puissance *= 2;
    }
    return numero;
}

std::string NumToNumBit(short unsigned int num) {
    std::string numeroBit;
    if (num > 256) throw AutoCellException("Numero d’automate indefini");
    unsigned short int p = 128;
    int i = 7;
    while (i >= 0) {
        if (num >= p) {
            numeroBit.push_back('1'); num -= p; }
        else {
                numeroBit.push_back('0'); }
        i--;
        p = p / 2;
    }
    return numeroBit;
}

Automate1D::Automate1D(std::string nom, short unsigned int n):numRegle(n), numBitRegle(NumToNumBit(n)){
    Automate::nomAutomate=nom;
    Automate::dimension=1;
    Automate::nombreEtats = 2;//2 etats possibles pour ses cellules
}//constructeur avec le n° de règle

Automate1D::Automate1D(std::string nom, const std::string &n):numRegle(NumBitToNum(n)){
    Automate::nomAutomate=nom;
    Automate::dimension=1;
    Automate::nombreEtats = 2;//2 etats possibles pour ses cellules
}//constructeur avec le n° binaire de la règle


unsigned int Automate1D::destin(unsigned int colonne, unsigned int ligne, const Etat &e) const{
    unsigned int conf = 0;
    if(colonne > 0)
        conf += e.getCellule(colonne-1)*4;
    if (colonne == 0)
        conf += e.getCellule(e.getTaille()-1)*4;

    conf += e.getCellule(colonne)*2;

    if(colonne < e.getTaille() - 1)
        conf += e.getCellule(colonne+1);
    if(colonne==e.getTaille()-1)
        conf += e.getCellule(0);

    if(numBitRegle[7-conf] - '0'){

         return 1;
    }
    else{
        return 0;
    }
}
