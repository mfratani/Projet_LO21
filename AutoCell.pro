QT += widgets
QT += xml
QMAKE_CXXFLAGS = -std=c++11
QMAKE_LFLAGS = -std=c++11

SOURCES += \
    main.cpp \
    simulateur.cpp \
    etat.cpp \
    automatesManager.cpp \
    jeudelavie.cpp \
    automateSpecifier.cpp \
    fenetreAjout.cpp \
    fenetreSommaire.cpp \
    fenetrechargement.cpp \
    automate1d.cpp \
    feudeforet.cpp \
    automate.cpp





HEADERS += \
    automate.h \
    etat.h \
    simulateur.h \
    automatesManager.h \
    autocellexception.h \
    JeuDeLaVie.h \
    automatespecifier.h \
    fenetreAjout.h \
    fenetreSommaire.h \
    fenetrechargement.h \
    automate1d.h \
    feudeforet.h





