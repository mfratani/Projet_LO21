#ifndef FENETRECHARGEMENT_H
#define FENETRECHARGEMENT_H

#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QTabWidget>
#include <QTableWidget>
#include <QLineEdit>
#include <QWidget>
#include <QComboBox>
#include <QSpinBox>
#include <QSlider>
#include <QStackedLayout>
#include <QStackedWidget>
#include <QHeaderView>
#include <QTimer>
#include "etat.h"
#include "simulateur.h"
#include <QIntValidator>
#include <QString>
#include <QTextStream>
#include <vector>

#define FENETRE_SIZE 800
#define TAILLE_MAX 500 //maximum de la taille d'un automate

class AutomateManager;
class Automate;

class FenetreChargement : public QWidget{
    Q_OBJECT
    private:
        QVBoxLayout *layoutPrincipal; /*!< Le layout principal*/
        QHBoxLayout *interfaceLayout; /*!< Le layout qui va contenir toute l'interface avec l'utilisateur*/
        QVBoxLayout *infosLayout; /*!< contient les layouts contenant les réglages concernant l'automate et contenant les règles */
        QHBoxLayout *reglagesLayout; /*!< contient les réglages de l'automate */
        QHBoxLayout *caracteristiqueAutomateLayout; /*!< Contient les caractéristiques communes des automates */
        QPushButton *simulerBouton; /*!< Bouton permettant de choisir l'automate à simuler */
        QVBoxLayout *menuLayout; /*!< Layout qui contient les boutons des diverses actions que l'utilisateur fait sur l'automate */
        QComboBox *choixAutomate; /*!< Menu déroulant pour choisir l'automate à simuler */
        QLabel *labelChoix;

    //Les boutons du menu -----------------------------------------------------------------------------------------
        QPushButton *nextBouton; /*!< bouton appelant le slot suivant permettant de générer le prochain état de l'automate et de l'afficher */
        QPushButton *runBouton; /*!< bouton appelant le slot run permettant de générer les états suivants de l'automate à une certaine vitesse jusqu'à un appuie sur le bouton pause */
        QPushButton *resetBouton; /*!< bouton appelant le slot reset qui permet de remettre l'automate à son état de départ */
        QPushButton *quitterBouton; /*!< bouton permettant de quitter la fenêtre chargement */
        QPushButton *pauseBouton; /*!< bouton appelant le slot pause qui permet d'arrêter le run */
        QPushButton *sauveBouton; /*!< bouton permettant de sauvegarder l'état en cours dans le fichier XML */

    //widgets de choix de la taille des états----------------------------------------------------------------
        QLabel *tailleLabel; /*!< label "taille" */
        QSpinBox *tailleSpinBox; /*!< spinbox permettant de choisir la taille de l'automate à simuler */

    //widgets de choix de la vitesse-------------------------------------------------------------------------
        QLabel *vitesseLabel; /*!< label "vitesse" */
        QSlider *vitesseSlider; /*!< slider permettant de régler la vitesse de simulation de l'automate */

    //Les donnees communes aux automates----------------------------------------------------------------------
        QLabel *nomAutomateLabel; /*!< label "nomAutomate" */
        QLabel *nomAutomateLabeldonnee; /*!< label contenant le nom de l'automate selectionné */
        QLabel *typeAutomateLabel; /*!< label "typeAutomate" */
        QLabel *typeAutomateLabeldonnee; /*!< label contenant le type de l'automate */
        QLabel *dimensionAutomateLabel; /*!< label "dimension" */
        QLabel *dimensionAutomateLabeldonnee; /*!< label contenant le dimension de l'automate */
        QLabel *nbEtatsAutomateLabel; /*!< label "nbEtats" */
        QLabel *nbEtatsAutomateLabeldonnee; /*!< label contenant le nombre d'états de l'automate */
        QStackedWidget *infosSpecifiqueAutomate; /*!< Contient les caractéristiques specifiques au type de l'automate */

    //les regles du JDV---------------------------------------------------------------------------------------
        QWidget *infosSpecifiqueJDVWidget; /*!< Widget qui va contenir les layouts contenant les informations spécifiques au type jeu de la vie */
        QHBoxLayout *infosSpecifiqueJDVLayout; /*!< layout contenant les informations spécifiques au jeu de la vie */
        QLabel *nbVoisinsMinLabel; /*!< label "nbVoisinsMin" */
        QLabel *nbVoisinsMinLabelDonnee; /*!< label contenant le nombre de voisins necessaires au minimum pour que la cellule survive */
        QLabel *nbVoisinsMaxLabel; /*!< label "nbVoisinsMax" */
        QLabel *nbVoisinsMaxLabelDonnee; /*!< label contenant le nombre de voisins necessaires au maximum pour que la cellule survive */
        QLabel *nbVoisinsVieLabel; /*!< label "bnVoisinsVie" */
        QLabel *nbVoisinsVieLabelDonnee; /*!< label contenant le nombre de voisins necessaires pour que la cellule renaisse */

    //les regles du jeu 1D-----------------------------------------------------------------------------
        QWidget *infosSpecifiqueD1Widget; /*!< Widget qui va contenir les layouts contenant les infos spécifiques au type 1D */
        QHBoxLayout *infosSpecifiqueD1Layout; /*!< layout contenant les informations spécifiques à l'automate 1D */
        QLabel *numRegleLabel; /*!< label "numRegle" */
        QLabel *numRegleLabelDonnee; /*!< label contenant le numéro de la règle en décimal de l'automate 1D */
        QLabel *numBitRegleLabel; /*!< label "numBitRegle" */
        QLabel *numBitRegleLabelDonnee; /*!< label contenant le numéro de la règle en bit de l'automate 1D */

    //Les regles du FDF-------------------------------------------------------------------------------------
        QWidget *infosSpecifiqueFDFWidget; /*!< Widget qui va contenir les layouts contenant les infos spécifiques au type feu de foret */
        QHBoxLayout *infosSpecifiqueFDFLayout; /*!< layout contenant les informations spécifiques au feu de forêt */
        QLabel* ventNordLabel;/*!< label "vent nord" */
        QLabel* ventSudLabel; /*!< label "vent sud" */
        QLabel* ventEstLabel; /*!< label "vent est" */
        QLabel* ventOuestLabel; /*!< label "vent ouest" */
        QLabel* ventNordLabelDonnee; /*!< indique si un vent nord existe ou non pour cet automate */
        QLabel* ventSudLabelDonnee; /*!< indique si un vent sud existe ou non pour cet automate */
        QLabel* ventEstLabelDonnee; /*!< indique si un vent est existe ou non pour cet automate */
        QLabel* ventOuestLabelDonnee; /*!< indique si un vent ouest existe ou non pour cet automate */

    //######################################Ajouter widget si nouveau type d'automate###############################

    //Affichage des cellules----------------------------------------------------------------
        QHBoxLayout* choix_depart_HLayout; /*!< Va contenir les différentes manières de générer la grille d'états de départ */
        QLabel* Indication_Label; /*!< Explique les différentes options qui s'offre à l'utilisateur */
        QPushButton *aleatoire_Bouton; /*!< Déclenche  le slot qui initialise la grille aléatoirement */
        QPushButton* etat_precedent_Bouton; /*!< Déclenche le chargement de l'ancien état de l'automate depuis le fichier XML */
        QPushButton* go_Bouton; /*!< Démarre la simulation, fait apparaitre le menu */

    //########################Génération d'états spécifique au type ##################################""

    //Pour le FDF -----------------------------------------------------------
        QPushButton *foret_complete_FDF_Bouton; /*!< Déclenche le slot permettant de remplir la forêt d'arbres (toutes les cellules dans l'état arbre */

        QHBoxLayout* grille_HLayout; /*!< Contient la grille */
        QTableWidget* grilleEtat; /*!< grille d'états des cellules */

        //stockage du simulateur :
        Simulateur* simulateur;
        bool continu; /*!< simulation lancée ou non */

        QTimer *timer; /*!< timer pour le slot run */

    public:
        explicit FenetreChargement(QWidget* parent=0);

        /*!
            *  \brief fonction qui permet de créer un état de départ pour un automate
            * à partir de l'affichage (de la grille de cellule)
            */
        void setEtatDepart(const Automate&, unsigned int);

        /*!
            *  \brief permet d'afficher un état à l'écran (remplissage de la grille)
            * grâce à un état transmis en argument
            */
        void afficherEtat(Etat &etat, unsigned int taille, unsigned int hauteur);

        /*!
         * \brief Fonction qui charge les automates du fichier XML str dans l'Automate Manager
         * \param str : nom du fichier XML
         */
        int chargementAutomates(const QString str);

        /*!
         *\fn void Sauvegarde_etat(const std::string& fichier, const std::string&, const Etat& )
         *  \brief écriture d'un état dans le fichier XML
         * \param fichier : nom du fichier XML
         */
        void Sauvegarde_etat(const std::string& fichier, const std::string&, const Etat& );

        /*!
         * \fn Etat Chargement_dernier_etat(const std::string& fichier, const Automate&)
         * \brief Chargement d'un état depuis le fichier XML
         * \param fichier : nom du fichier XML
         */
        Etat Chargement_dernier_etat(const std::string& fichier, const Automate&);

    public slots:
        /*!
            *  \brief Slot sélectionnant l'automate à simuler
            *
            * permet d'afficher les layouts horizontaux de grille et de choix d'état de départ
            */
        void generationEtatDepart();

        /*!
            *  \brief Slot permettant d'afficher les informations d'un automate
            * selectionné par l'utilisateur
            */
        void afficherInfosAutomate(QString nom);

        /*!
            *  \brief Appelle la fonction d'affichage d'un état
            * (afficherEtat())
            */
        void afficherEtatSlot();

        /*!
            *  \brief Slot permettant d'activer les cellules de la grille en double cliquant dessus
            * (change l'état d'une cellule)
            */
        void Activation(const QModelIndex&);

        /*!
            *  \brief Slot appelant la fonction responsable du chargement de l'ancien état de l'automate (enregistré dans le fichier "testXML.xml")
            */
        void Recuperer_ancien_etat();

        /*!
            *  \brief Slot fermant la fenêtre de chargement et vidant l'AutomateManager
            */
        void fermetureFenetreChargement();

        /*!
            *  \brief Slot permettant de générer aléatoirement un état de départ
            */
        void Aleatoire();

        /*!
            *  \brief Slot qui génère une forêt complète pour le FDF
            *
            * place toutes les cellules dans l'état d'arbre (vert)
            */
        void foret_complete_FDF();

        /*!
            *  \brief Slot qui lance la simulation
            *
            * l'état de départ ne pourra plus changer
            */
        void lancerSimulation();

        /*!
            *  \brief Slot permettant d'afficher l'état suivant
            */
        void etatSuivant();

        /*!
            *  \brief Slot permettant d'afficher les états suivants à une certaine vitesse
            *
            * jusqu'à ce que l'on appuie sur le bouton pause
            */
        void run();

        /*!
            *  \brief Slot permettant de mettre en pause le run
            */
        void pause();

        /*!
            *  \brief Slot appelant la fonction responsable de la sauvegarde de l'état actuel de l'automate dans le fichier "testXML.xml"
            */
        void save();

        /*!
            *  \brief Slot permettant de remettre l'automate à son état de départ
            */
        void reset();
};

#endif // FENETRECHARGEMENT_H


