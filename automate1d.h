#ifndef AUTOMATE1D_H
#define AUTOMATE1D_H
#include <string>
#include "automate.h"

class Automate;
class AutomateManager;

/*! \class Automate1D
   * \brief Classe de l'automate cellulaire élémentaire à une dimension
   *
   * Hérite de la classe automate
   */
class Automate1D: public Automate{
    private:
        short unsigned int numRegle; /*!< numéro de la règle de transition en décimal */
        std::string numBitRegle; /*!< numéro de la règle de transition en bit */

        /*!
            *  \brief Constructeur 1 avec le numéro de la règle en décimal
            *
            * tous les constructeurs sont privés car la gestion des automates 1D se fait avec AutomateManager
            *
            *  \param nom : nom de l'automate
            *   \param n : numéro de la règle en décimal
            */
        Automate1D(std::string nom, short unsigned int n);

        /*!
            * \brief Constructeur 2 avec le numéro de la règle en bit
            *
            * tous les constructeurs sont privés car la gestion des automates 1D se fait avec AutomateManager
            *
            *  \param nom : nom de l'automate
            *  \param n : numéro de la règle en bit
            */
        Automate1D(std:: string nom, const std::string &n);

        /*!
            * \brief Constructeur par recopie
            *
            * privé car la gestion des automates 1D se fait avec AutomateManager
            */
        Automate1D (const Automate1D& a) = default;

        /*!
            * \brief Destructeur
            *
            * privé car la gestion des automates 1D se fait avec AutomateManager
            */
        ~Automate1D()= default;

    public:
        /*!
         * \brief Renvoie le prochain état de la cellule de coordonnées[ligne][colonne]
         */
       virtual unsigned int destin( unsigned int colonne, unsigned int ligne, const Etat& e)const;

        /*!
            * \brief accesseur pour le numéro de la règle en decimal
            * \return numRegle
            */
        short unsigned int getNumRegle() const {return numRegle;}

        /*!
            * \brief accesseur pour le numéro de la règle en bit
            * \return numBitRegle
            */
        const std::string& getNumBitRegle() const {return numBitRegle;}

        /*!
            * \brief retourne le type de l'automate : D1
            * \return D1
            */
        std::string getType() const {return "D1";}

        friend class AutomateManager;/*!< pour permettre au Manager d'utiliser le constructeur privé des automates 1D */
};

/*!
 * \brief conversion d'un entier décimal en bit
 * \param num : l'entier à convertir
 */
std::string NumToNumBit(short unsigned int num);

/*!
 * \brief conversion d'une chaine de caractère en entier décimal correspondant
 * \param num : chaîne de caractères en bit à convertir
 */
short unsigned int NumBitToNum(const std::string& num);

#endif // AUTOMATE1D_H
