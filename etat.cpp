#include <iostream>
#include "etat.h"
#include "automate.h"
#include "autocellexception.h"

Etat::Etat(unsigned int t, const Automate& a): taille(t), dimension(a.getDimension()){
    if(dimension==1){//Initialisation d'un tableau d'entier si automate de dimension 1
        valeur = new int*[t]; //Valeur pointe sur un tableau de pointeurs d'entiers de taille t
        for(unsigned int i = 0;i<t;i++){
            //Pour chacune des cases, on alloue de la mémoire pour un entier
            valeur[i]= new int;
            *valeur[i]=0;//On initialise les valeurs à 0
        }
    }
    if(dimension==2){//Initialisation d'un tableau de tableau d'e bool d'entier en cas d'automate de dimension 2
        valeur = new int*[t];//Valeur pointe sur une tableau de pointeur d'entier de taille t
        for(unsigned int i=0;i<t;i++){
            valeur[i]=new int[t];//Pour chacune des cases, on alloue de la mémoire pour un tableau d'entier de taille t
        }
        //On obtient donc une matrice d'entier à 2 dimensions
        for(unsigned int i=0;i<taille;i++){
            for(unsigned int j=0;j<taille;j++){
                valeur[i][j]=0;//On initialise toutes les valeurs à 0
            }
        }
    }
}

Etat::~Etat(){
    for(unsigned int i=0;i<taille;i++){
        delete valeur[i];
    }
    delete[] valeur;
}

void Etat::setCellule(int val, unsigned int colonne, unsigned int ligne){
    if(dimension==1){//Si une seule dimension, on ne prend pas en compte la ligne
        *valeur[colonne]= val;
    }
    else{
        valeur[ligne][colonne]= val;
    }
}

int Etat::getCellule(unsigned int colonne, unsigned int ligne)const{
    if(colonne<getTaille()&&ligne<getTaille()){
        if(dimension==1){//Idem que la fonction précédente
            return *valeur[colonne];
        }
        if(dimension==2){
            return valeur[ligne][colonne];
        }
    }
}

Etat& Etat::operator=(const Etat& e){//Surcharge de l'operateur d'affectation pour deux etats
    if(dimension != e.dimension || taille!=e.taille){//On vérifie que la dimension et la taille des etats sont identiques
        //Si ce n'est pas le cas, il faut changer les attributs de l'etat, et donc redéclarer valeur avec la nouvelle dimension et taille
        dimension = e.getDimension();
        taille = e.getTaille();
        int **newValeur; //Pointeur sur le nouveau tableau de valeur que contiendra l'état
        int ** oldValeur = valeur; //Pointeur sur le tableau valeur courant, pour pouvoir desallouer sa mémoire une fois le changement de tableau effectué
        //en fonction de la nouvelle dimension et de la nouvelle taille, on va reconstruire le nouveau tableau valeur
        //On procède comme dans le constructeur d'etat
        if(dimension == 1){
            newValeur = new int*[taille];
            for(unsigned int i=0;i<taille;i++){
                newValeur[i] = new int;
                newValeur[i] = 0;
            }
        }
        if(dimension == 2){
            newValeur = new int*[taille];
            for(unsigned int i=0;i<taille;i++){
                newValeur[i] = new int[taille];
            }
            for(unsigned int i=0;i<taille;i++){
                for(unsigned int j=0;j<taille;j++){
                    newValeur[i][j] = 0;
                }
            }
        }
        valeur = newValeur;
        delete[] oldValeur;
    }
    if(this!=&e){ //On veut empecher qu'un etat puisse s'affecter lui-même
        if(dimension==1){
            int* tmp;//Variable temporaire pour recuperer la valeur d'une case dans le tableau de l'etat e
            for(unsigned int i=0;i<taille;i++){
                tmp=e.valeur[i];
                (*valeur[i])=*tmp;
            }
        }
        if(dimension==2){
            for(unsigned int i=0;i<taille;i++){
                for(unsigned int j=0;j<taille;j++){
                    valeur[i][j]=e.valeur[i][j];
                }
            }
        }
    }
    return *this;
}

Etat::Etat(const Etat &e):taille(e.getTaille()), dimension(e.getDimension()){//Le constructeur par recopie
    //analogue au constructeur standard, sauf que l'on recupere les valeurs d'un etat e
    if(dimension==1){
        valeur = new int*[taille];
        for(unsigned int i=0; i<taille;i++){
            valeur[i] = new int;
            *valeur[i]= e.getCellule(i);
        }
    }
    if(dimension==2){
        valeur = new int*[taille];
        for(unsigned int i=0;i<taille;i++){
            valeur[i]=new int[taille];
        }
        for(unsigned int i=0;i<taille;i++){
            for(unsigned int j=0;j<taille;j++){
                valeur[i][j]=e.getCellule(j,i);
            }
        }
    }
}

