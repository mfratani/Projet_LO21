#ifndef SAUVEGARDE_H
#define SAUVEGARDE_H
#include <string>
#include "fenetrechargement.h"

class Automate;
class Etat;



/*!
 *\fn void Sauvegarde_etat(const std::string& fichier, const std::string&, const Etat& )
 *  \brief écriture d'un état dans le fichier XML
 * \param fichier : nom du fichier XML
 */
void Sauvegarde_etat(const std::string& fichier, const std::string&, const Etat& );



/*!
 * \fn Etat Chargement_dernier_etat(const std::string& fichier, const Automate&)
 * \brief Chargement d'un état depuis le fichier XML
 * \param fichier : nom du fichier XML
 */
Etat Chargement_dernier_etat(const std::string& fichier, const Automate&);


#endif // SAUVEGARDE_H
