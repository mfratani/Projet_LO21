#include "fenetrechargement.h"
#include "automatesManager.h"
#include "automatespecifier.h"
#include "automate.h"
#include "fenetrechargement.h"
#include "simulateur.h"
#include "JeuDeLaVie.h"
#include "automate1d.h"
#include "feudeforet.h"
#include "etat.h"
#include "autocellexception.h"
#include <QHeaderView>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <QtXml>
#include <QMessageBox>
#include <QTimer>
#include <QTextStream>
#include <vector>

FenetreChargement::FenetreChargement(QWidget *parent){
    AutomateManager& manager = AutomateManager::getAutomateManager();


    chargementAutomates("automates.xml");


//Declaration des layouts----------------------------------------------------------------------------
    layoutPrincipal = new QVBoxLayout();
    this->setLayout(layoutPrincipal);
    interfaceLayout = new QHBoxLayout();
    infosLayout = new QVBoxLayout();
    reglagesLayout = new QHBoxLayout();
    caracteristiqueAutomateLayout = new QHBoxLayout();
    menuLayout = new QVBoxLayout();

//Le menu déroulant ----------------------------------------------------------------------------------
    choixAutomate=new QComboBox;
    AutomateManager::IteratorManager i = manager.getIteratorManager();
    while(!i.isDone()){ //On itere sur l'AM pour recuperer tous les noms des automates qui seront selectionnables dans le menu déroulant
        choixAutomate->addItem(QString::fromStdString(i.current().getNomAutomate()));
        i.next();
    }
    connect(choixAutomate, SIGNAL(currentIndexChanged(QString)), this, SLOT(afficherInfosAutomate(QString)));

//Les infos relatives à la simulation --------------------------------------------------------------------
    labelChoix=new QLabel("Choix de l automate: ");
    tailleLabel = new QLabel("Taille: ");
    tailleSpinBox = new QSpinBox();
    tailleSpinBox->setRange(5,TAILLE_MAX);
    tailleSpinBox->setValue(25);
    vitesseLabel = new QLabel("Vitesse: ");
    vitesseSlider = new QSlider(Qt::Horizontal);
    vitesseSlider->setRange(1,15);
    simulerBouton = new QPushButton("Choisir état départ");
    quitterBouton=new QPushButton("Quitter");
    connect(simulerBouton,SIGNAL(clicked(bool)), this, SLOT(generationEtatDepart()));
    connect(quitterBouton, SIGNAL(clicked(bool)), this, SLOT(fermetureFenetreChargement()));
    reglagesLayout->addWidget(labelChoix);
    reglagesLayout->addWidget(choixAutomate);
    reglagesLayout->addWidget(tailleLabel);
    reglagesLayout->addWidget(tailleSpinBox);
    reglagesLayout->addWidget(vitesseLabel);
    reglagesLayout->addWidget(vitesseSlider);
    reglagesLayout->addWidget(simulerBouton);
    infosLayout->SetFixedSize;
    reglagesLayout->addWidget(quitterBouton);

// Menu ---------------------------------------------------------------------------------------------
    nextBouton=new QPushButton("Next");
    runBouton=new QPushButton("Run");
    resetBouton=new QPushButton("Reset");
    pauseBouton=new QPushButton("Pause");
    sauveBouton = new QPushButton("Enregistrer l'état actuel");
    menuLayout->addWidget(nextBouton);
    menuLayout->addWidget(runBouton);
    menuLayout->addWidget(pauseBouton);
    menuLayout->addWidget(resetBouton);
    menuLayout->addWidget(sauveBouton);

//Les infos communes à tous les automates --------------------------------------------------------------
    nomAutomateLabel = new QLabel("Nom:");
    nomAutomateLabeldonnee = new QLabel;
    typeAutomateLabel = new QLabel("Type:");
    typeAutomateLabeldonnee = new QLabel;
    dimensionAutomateLabel = new QLabel("Dimension:");
    dimensionAutomateLabeldonnee = new QLabel;
    nbEtatsAutomateLabel = new QLabel("Nombre d'état:");
    nbEtatsAutomateLabeldonnee = new QLabel;
    caracteristiqueAutomateLayout->addWidget(nomAutomateLabel);
    caracteristiqueAutomateLayout->addWidget(nomAutomateLabeldonnee);
    caracteristiqueAutomateLayout->addWidget(typeAutomateLabel);
    caracteristiqueAutomateLayout->addWidget(typeAutomateLabeldonnee);
    caracteristiqueAutomateLayout->addWidget(dimensionAutomateLabel);
    caracteristiqueAutomateLayout->addWidget(dimensionAutomateLabeldonnee);
    caracteristiqueAutomateLayout->addWidget(nbEtatsAutomateLabel);
    caracteristiqueAutomateLayout->addWidget(nbEtatsAutomateLabeldonnee);

//Les infos spécifiques au type de l'automate-----------------------------------------
    infosSpecifiqueAutomate = new QStackedWidget();

//######ajouter nouveau type

//Pour JDV
    infosSpecifiqueJDVLayout = new QHBoxLayout;
    infosSpecifiqueJDVWidget = new QWidget;
    infosSpecifiqueJDVWidget->setLayout(infosSpecifiqueJDVLayout);
    nbVoisinsMinLabel = new QLabel("Nombre de voisins Min pour la survie:");
    nbVoisinsMinLabelDonnee = new QLabel();
    nbVoisinsMaxLabel = new QLabel("Nombre de voisins Max pour la survie:");
    nbVoisinsMaxLabelDonnee = new QLabel();
    nbVoisinsVieLabel = new QLabel("Nombre de voisins pour creer une cellule");
    nbVoisinsVieLabelDonnee =  new QLabel();
    infosSpecifiqueJDVLayout->addWidget(nbVoisinsMinLabel);
    infosSpecifiqueJDVLayout->addWidget(nbVoisinsMinLabelDonnee);
    infosSpecifiqueJDVLayout->addWidget(nbVoisinsMaxLabel);
    infosSpecifiqueJDVLayout->addWidget(nbVoisinsMaxLabelDonnee);
    infosSpecifiqueJDVLayout->addWidget(nbVoisinsVieLabel);
    infosSpecifiqueJDVLayout->addWidget(nbVoisinsVieLabelDonnee);

//Pour D1
    infosSpecifiqueD1Layout = new QHBoxLayout;
    infosSpecifiqueD1Widget = new QWidget;
    infosSpecifiqueD1Widget->setLayout(infosSpecifiqueD1Layout);
    numRegleLabel = new QLabel("NumRegle:");
    numRegleLabelDonnee = new QLabel;
    numBitRegleLabel = new QLabel("NumBitRegle:");
    numBitRegleLabelDonnee = new QLabel;
    infosSpecifiqueD1Layout->addWidget(numRegleLabel,1);
    infosSpecifiqueD1Layout->addWidget(numRegleLabelDonnee,1);
    infosSpecifiqueD1Layout->addWidget(numBitRegleLabel,1);
    infosSpecifiqueD1Layout->addWidget(numBitRegleLabelDonnee,1);

//Pour FDF
    infosSpecifiqueFDFLayout = new QHBoxLayout;
    infosSpecifiqueFDFWidget = new QWidget;
    infosSpecifiqueFDFWidget->setLayout(infosSpecifiqueFDFLayout);
    ventNordLabel = new QLabel("Vent Nord");
    ventSudLabel = new QLabel("Vent Sud");
    ventEstLabel = new QLabel("Vent Est");
    ventOuestLabel = new QLabel("Vent Ouest");
    ventNordLabelDonnee = new QLabel();
    ventSudLabelDonnee = new QLabel();
    ventEstLabelDonnee = new QLabel();
    ventOuestLabelDonnee = new QLabel();

//####### Ajout d'un type d'automate #######

    infosSpecifiqueAutomate->addWidget(infosSpecifiqueJDVWidget);
    infosSpecifiqueAutomate->addWidget(infosSpecifiqueD1Widget);
    infosSpecifiqueAutomate->addWidget(infosSpecifiqueFDFWidget);

//Positionnement des layouts ------------------------------------------------------

    layoutPrincipal->addLayout(interfaceLayout);
    interfaceLayout->addLayout(infosLayout);
    infosLayout->addLayout(reglagesLayout);
    infosLayout->addLayout(caracteristiqueAutomateLayout);
    infosLayout->addWidget(infosSpecifiqueAutomate);

//Affichage des cellules ----------------------------------------------------------
    choix_depart_HLayout = new QHBoxLayout();
    aleatoire_Bouton = new QPushButton("Aléatoire");
    //###clique sur aleatoire_Bouton => colorer aléatoirement la grille uniquement

    etat_precedent_Bouton = new QPushButton("Etat précédent");
    //##clique sur etat_precedent_Bouton => chargement de l'ancien état depuis le fichier Xml

    foret_complete_FDF_Bouton = new QPushButton("Foret Complete"); //Bouton pour générer une foret pour un FDF
    Indication_Label = new QLabel("Comment souhaitez-vous créer l'état de départ ? ");
    grille_HLayout = new QHBoxLayout(); //va contenir affichage des cellules
    go_Bouton = new QPushButton("Démarrer la simulation");
    //clique sur go_bouton => disparition de choix_depart_HLayout
                            // + apparition menuQVLayout
                            // blocage avec triggers

    grilleEtat = new QTableWidget();
    timer = new QTimer(this);
}

void FenetreChargement::afficherInfosAutomate(QString nom){
    AutomateManager& m = AutomateManager::getAutomateManager(); //On recupere l'AM
    AutomateManager::IteratorManager i = m.getIteratorManager(); //On va iterer sur l'AM
    std::string n = i.current().getNomAutomate();
    while(QString::fromStdString(i.current().getNomAutomate())!=nom)
        i.next();
    Automate& a = i.current(); //On recupere l'automate qui a le même nom que celui selectionné
  //On affiche les caractéristiques de l'automate commune à tous les types
    nomAutomateLabeldonnee->setText(QString::fromStdString(a.getNomAutomate()));
    nbEtatsAutomateLabeldonnee->setText(QString::number(a.getNbEtats()));
    dimensionAutomateLabeldonnee->setText(QString::number(a.getDimension()));
    std::string typeAutomate = a.getType();
  //Selon le type de l'automate, on affiche les regles
        if(typeAutomate=="JDV"){
            typeAutomateLabeldonnee->setText("Jeu_de_la_vie");
            infosSpecifiqueAutomate->setCurrentIndex(0);
            nbVoisinsMinLabelDonnee->setText(QString::number(dynamic_cast<JeuDeLaVie&>(a).getNbVoisinsMin()));
            nbVoisinsMaxLabelDonnee->setText(QString::number(dynamic_cast<JeuDeLaVie&>(a).getNbVoisinsMax()));
            nbVoisinsVieLabelDonnee->setText(QString::number(dynamic_cast<JeuDeLaVie&>(a).getNbVoisinsVie()));
        }
        if(typeAutomate == "D1"){
            typeAutomateLabeldonnee->setText("Automate_1D");
            infosSpecifiqueAutomate->setCurrentIndex(1);
            numRegleLabelDonnee->setText(QString::number(dynamic_cast<Automate1D&>(a).getNumRegle()));
            numBitRegleLabelDonnee->setText(QString::fromStdString(dynamic_cast<Automate1D&>(a).getNumBitRegle()));
        }
        if(typeAutomate=="FDF"){
            typeAutomateLabeldonnee->setText("Feu_De_Foret");
            infosSpecifiqueAutomate->setCurrentIndex(2);
            if(dynamic_cast<FeuDeForet&>(a).getVentNord()){
                ventNordLabelDonnee->setText("OUI");
            }
            else{
                ventNordLabelDonnee->setText("NON");
            }
            if(dynamic_cast<FeuDeForet&>(a).getVentSud()){
                ventSudLabelDonnee->setText("OUI");
            }
            else{
                ventSudLabelDonnee->setText("NON");
            }
            if(dynamic_cast<FeuDeForet&>(a).getVentEst()){
                ventEstLabelDonnee->setText("OUI");
            }
            else{
                ventEstLabelDonnee->setText("NON");
            }
            if(dynamic_cast<FeuDeForet&>(a).getVentOuest()){
                ventOuestLabelDonnee->setText("OUI");
            }
            else{
                ventOuestLabelDonnee->setText("NON");
            }
            infosSpecifiqueFDFLayout->addWidget(ventNordLabel);
            infosSpecifiqueFDFLayout->addWidget(ventNordLabelDonnee);
            infosSpecifiqueFDFLayout->addWidget(ventSudLabel);
            infosSpecifiqueFDFLayout->addWidget(ventSudLabelDonnee);
            infosSpecifiqueFDFLayout->addWidget(ventEstLabel);
            infosSpecifiqueFDFLayout->addWidget(ventEstLabelDonnee);
            infosSpecifiqueFDFLayout->addWidget(ventOuestLabel);
            infosSpecifiqueFDFLayout->addWidget(ventOuestLabelDonnee);
        }
   }


void FenetreChargement::generationEtatDepart(){
    //récupération du nom de l'automate voulu
    std::string nom = ((choixAutomate->currentText())).toStdString();
    AutomateManager& manager = AutomateManager::getAutomateManager(); //on récupère l'AM
    AutomateManager::IteratorManager i = manager.getIteratorManager();
     while((i.current().getNomAutomate())!=nom)
       i.next();
    Automate& a = i.current();
    choix_depart_HLayout->addWidget(Indication_Label);
    choix_depart_HLayout->addWidget(aleatoire_Bouton);
    choix_depart_HLayout->addWidget(etat_precedent_Bouton);
    connect(aleatoire_Bouton,SIGNAL(clicked(bool)), this, SLOT(Aleatoire()));
 // Genere une grille aléatoire avec le Slot Aléatoire
    connect(etat_precedent_Bouton,SIGNAL(clicked(bool)), this, SLOT(Recuperer_ancien_etat()));
 // Active le slot qui va appeler la fonction responsable du chargement de l'ancien état de l'automate

//Methode de genération d'état spécfique au FDF --------------------
    if(a.getType()=="FDF"){
        choix_depart_HLayout->addWidget(foret_complete_FDF_Bouton);
        connect(foret_complete_FDF_Bouton, SIGNAL(clicked(bool)), this, SLOT(foret_complete_FDF()));
    }
    choix_depart_HLayout->addWidget(go_Bouton);
    connect(go_Bouton,SIGNAL(clicked(bool)), this, SLOT(lancerSimulation()));
    // Active le slot LancerSimulation

//Methode de genération d'état spécfique au FDF --------------------
    layoutPrincipal->addLayout(choix_depart_HLayout);
    layoutPrincipal->addLayout(grille_HLayout);
    unsigned int taille = tailleSpinBox->value();
    FenetreChargement::setEtatDepart(a, taille);
}

void FenetreChargement::setEtatDepart(const Automate& a, unsigned int taille)
{
    unsigned int hauteur = taille;
    if(a.getDimension() == 1)
    {
       hauteur=1;
       resize(FENETRE_SIZE,FENETRE_SIZE-800);
    }
    grilleEtat->setRowCount(hauteur);
    grilleEtat->setColumnCount(taille);
    if(a.getDimension() == 1)
         grilleEtat->setFixedSize(25*taille, 25*hauteur + 25);
    else
        grilleEtat->setFixedSize(25*taille, 25*hauteur);

    //redimensionnement de la grille si elle dépasse de l'écran
    if(grilleEtat->width() > 1500)
    {
        if(a.getDimension() == 1)
             grilleEtat->setFixedSize(25*taille + (1500 - grilleEtat->width()), 25*hauteur + 25);
        else
             grilleEtat->setFixedSize(25*taille + (1500 - grilleEtat->width()), 25*hauteur);
    }
    if(grilleEtat->height() > 750)
    {
        if(a.getDimension() == 1)
             grilleEtat->setFixedSize(grilleEtat->width(), 25*hauteur + 25 + (750 - grilleEtat->height()));
        else
             grilleEtat->setFixedSize(grilleEtat->width(), 25*hauteur + (750 - grilleEtat->height()));
    }

    grilleEtat->horizontalHeader()->setVisible(false);
    grilleEtat->verticalHeader()->setVisible(false);
    for (unsigned int j=0; j<hauteur;++j)
    {
       for (unsigned int i=0; i<taille;++i)
       {
           grilleEtat->setRowHeight(j,25);
           grilleEtat->setColumnWidth(i,25);
           grilleEtat->setItem(j,i,new QTableWidgetItem(QString::number(0)));
           grilleEtat->item(j,i)->setBackgroundColor("white");
           grilleEtat->item(j,i)->setTextColor("white");
       }
    }
    grille_HLayout->addWidget(grilleEtat);//Affiche la grille au layout horizontal en bas de la fenetre
    connect(grilleEtat,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(Activation(QModelIndex)));
    // Active la coloration manuel de la cellule grace au slot Activation

    //on desactive le choix de la taille et de l'automate
        tailleSpinBox->setReadOnly(true);
        choixAutomate->setEnabled(false);
}


void FenetreChargement::Activation(const QModelIndex& index)
{
    AutomateManager& manager = AutomateManager::getAutomateManager(); //on récupère l'AM
    AutomateManager::IteratorManager it = manager.getIteratorManager();

    //récupération du nom de l'automate voulu
    std::string nom = ((choixAutomate->currentText())).toStdString();
    while((it.current().getNomAutomate())!=nom)
        it.next();
    Automate& a = it.current();
    unsigned int nbEtats = a.getNbEtats(); //On recupere le nombre d'etat possible
    std::string type = a.getType();
    std::string str = grilleEtat->item(index.row(),index.column())->text().QString::toStdString(); //On recupere la Qstring dans la cellule, et on la convertit en string

    unsigned int numEtat = std::stoi(str);//Ensuite on la convertit en int pour récuperer le numero de l'état

        //Les couleurs specifiques du FDF -------------------------------------------------------
           if(type=="FDF"){
               if (numEtat == 0) { //Si la cellule est vide, on passe à l'etat 1
                   grilleEtat->item(index.row(),index.column())->setText(QString::number(1)); //On met 1 dans la cellule
                   grilleEtat->item(index.row(),index.column())->setBackgroundColor("green"); //On colore en vert
                   grilleEtat->item(index.row(),index.column())->setTextColor("green");
                }
           if (numEtat == 1) { //Si l'etat est 1, on passe à cendre
               grilleEtat->item(index.row(),index.column())->setText(QString::number(2)); //On met 1 dans la cellule
               grilleEtat->item(index.row(),index.column())->setBackgroundColor("grey"); //On colore en vert
               grilleEtat->item(index.row(),index.column())->setTextColor("grey");
           }
           if (numEtat== 2) { //Si l'etat est 2, on passe à feu
               grilleEtat->item(index.row(),index.column())->setText(QString::number(3)); //On met 2 dans la cellule
               grilleEtat->item(index.row(),index.column())->setBackgroundColor("red"); //On colore en gris
               grilleEtat->item(index.row(),index.column())->setTextColor("red");
           }
           if (numEtat == 3) { //Si l'etat est 3, on passe à vide
               grilleEtat->item(index.row(),index.column())->setText(QString::number(0)); //On met 3 dans la cellule
               grilleEtat->item(index.row(),index.column())->setBackgroundColor("white"); //On colore en red
               grilleEtat->item(index.row(),index.column())->setTextColor("white");
           }
        }
    //Les couleurs standards pour un automate---------------------------------------------
    else{
        if(numEtat==0){ //Si on est à l'état 0, on passe l'état à 1 et la case à noire
            grilleEtat->item(index.row(),index.column())->setText(QString::number(1));
            grilleEtat->item(index.row(),index.column())->setBackgroundColor(Qt::GlobalColor(2));
            grilleEtat->item(index.row(),index.column())->setTextColor(Qt::GlobalColor(2));
        }
        else if(numEtat==nbEtats-1) //Si on atteint l'etat max, on revient à blanc
        {
            grilleEtat->item(index.row(),index.column())->setText(QString::number(0));
            grilleEtat->item(index.row(),index.column())->setBackgroundColor(Qt::GlobalColor(3));
            grilleEtat->item(index.row(),index.column())->setTextColor(Qt::GlobalColor(3));
        }
        else{ //Sinon, on incrémente le numero de l'état et on passe la case à la couleur suivante
            grilleEtat->item(index.row(),index.column())->setText(QString::number(numEtat+1));
            grilleEtat->item(index.row(),index.column())->setBackgroundColor(Qt::GlobalColor(numEtat+2+1));
            grilleEtat->item(index.row(),index.column())->setTextColor(Qt::GlobalColor(numEtat+2+1));
        }
    }
}

void FenetreChargement::fermetureFenetreChargement(){
    AutomateManager::getAutomateManager().viderAutomateManager();
    close();
}


void FenetreChargement::Aleatoire()
{
    AutomateManager& manager = AutomateManager::getAutomateManager(); //on récupère l'AM
    AutomateManager::IteratorManager it = manager.getIteratorManager();
    //récupération du nom de l'automate voulu
    std::string nom = ((choixAutomate->currentText())).toStdString();
    while((it.current().getNomAutomate())!=nom)
       it.next();
   Automate& a = it.current();
   unsigned int nbEtats = a.getNbEtats();
   unsigned int taille = tailleSpinBox->value();
   unsigned int hauteur = taille;
   int numEtat = 0;
   srand(time(NULL));
   if(a.getDimension()==1)
       hauteur=1;
   std::string type = a.getType();
   for (unsigned int j=0; j<hauteur;++j)
   {
       for (unsigned int i=0; i<taille;++i)
       {
           numEtat = rand()%nbEtats;
          //Les couleurs spécifiques du FDF
           if(type=="FDF"){
               if (numEtat == 0) { //Si l'etat est 0
                   grilleEtat->item(j, i)->setText(QString::number(0)); //On met 0 dans la cellule
                   grilleEtat->item(j, i)->setBackgroundColor(Qt::GlobalColor(3)); //On colore en blanc
                   grilleEtat->item(j, i)->setTextColor(Qt::GlobalColor(3));
                }
               if (numEtat == 1) { //Si l'etat est 1
                   grilleEtat->item(j, i)->setText(QString::number(1)); //On met 1 dans la cellule
                   grilleEtat->item(j, i)->setBackgroundColor("green"); //On colore en vert
                   grilleEtat->item(j, i)->setTextColor("green");
               }
               if (numEtat == 2) { //Si l'etat est 2
                   grilleEtat->item(j, i)->setText(QString::number(2)); //On met 2 dans la cellule
                   grilleEtat->item(j, i)->setBackgroundColor("grey"); //On colore en gris
                   grilleEtat->item(j, i)->setTextColor("grey");
               }
               if (numEtat == 3) { //Si l'etat est 3
                   grilleEtat->item(j, i)->setText(QString::number(3)); //On met 3 dans la cellule
                   grilleEtat->item(j, i)->setBackgroundColor("red"); //On colore en red
                   grilleEtat->item(j, i)->setTextColor("red");
               }
        }
    //Les couleurs standard -------------------------------------------------------
       else{
           if (numEtat == 0)
           {
               grilleEtat->item(j,i)->setText(QString::number(0));
               grilleEtat->item(j,i)->setBackgroundColor(Qt::GlobalColor(3));//Blanc
               grilleEtat->item(j,i)->setTextColor(Qt::GlobalColor(3));
           }
           else if(numEtat == 1)
           {
               grilleEtat->item(j,i)->setText(QString::number(1));
               grilleEtat->item(j,i)->setBackgroundColor(Qt::GlobalColor(2));//Noir
               grilleEtat->item(j,i)->setTextColor(Qt::GlobalColor(2));
           }
           else
           {
               grilleEtat->item(j,i)->setText(QString::number(numEtat));
               grilleEtat->item(j,i)->setBackgroundColor(Qt::GlobalColor(numEtat+2));
               grilleEtat->item(j,i)->setTextColor(Qt::GlobalColor(numEtat+2));
           }
       }

     }
   }
}

void FenetreChargement::Recuperer_ancien_etat()
{
    AutomateManager& manager = AutomateManager::getAutomateManager(); //on récupère l'AM
    AutomateManager::IteratorManager it = manager.getIteratorManager();

    //récupération du nom de l'automate voulu
    std::string nom = ((choixAutomate->currentText())).toStdString();
    while((it.current().getNomAutomate())!=nom)
       it.next();
    Automate& a = it.current(); //on recupere l'automate selectionné
   //Récupération de l'ancien état
    try
   {
        Etat e = Chargement_dernier_etat("automates.xml",a);

        unsigned int nb_ligne;
        if (e.getDimension()==1)
            nb_ligne = 1;
        else nb_ligne = e.getTaille();
        //Réadaptation de GrilleEtat et de la valeur de la SpinBox
        tailleSpinBox->setValue(e.getTaille());
        setEtatDepart(a,e.getTaille());
        //Affichage de l'état lu depuis le fichier XML
        afficherEtat(e,e.getTaille(),nb_ligne);
    }
    catch (AutoCellException& ex)
        {
            std::cout<<ex.getInfo()<<endl;
            QMessageBox::warning(this, "Erreur", QString::fromStdString(ex.getInfo()));
        }
}

void FenetreChargement::lancerSimulation()
{
    interfaceLayout->addLayout(menuLayout);
    AutomateManager& manager = AutomateManager::getAutomateManager(); //on récupère l'AM
    AutomateManager::IteratorManager it = manager.getIteratorManager();
    //récupération du nom de l'automate voulu
    std::string nom = ((choixAutomate->currentText())).toStdString();
    while((it.current().getNomAutomate())!=nom)
       it.next();
    Automate& a = it.current(); //on recupere l'automate selectionné
    unsigned int taille = tailleSpinBox->value(); //taille choisi de l'automate
    unsigned int hauteur = taille; //on considère un carré pour les automates 2 dimensions
    if(a.getDimension()==1) //si une dimension, on a juste une ligne
       hauteur=1;
    disconnect(grilleEtat,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(Activation(QModelIndex))); //modification impossible de la grille
    Etat* etat_depart = new Etat(taille, a);
    // creation de l'etat en fonction de la grille de de depart
    for(unsigned int ligne = 0; ligne<hauteur; ligne++)
    {
        for(unsigned int colonne = 0; colonne < taille; colonne++) {
            unsigned int numEtat = grilleEtat->item(ligne,colonne)->text().toInt();
            etat_depart->setCellule(numEtat, colonne,ligne);
        }
    }

    //creation du simulateur :
    Simulateur* simulateur = new Simulateur(a, *etat_depart, 2); //taille du buffer a 2
    this->simulateur = simulateur;
    connect(nextBouton,SIGNAL(clicked(bool)), this, SLOT(etatSuivant()));
    connect(runBouton,SIGNAL(clicked(bool)), this, SLOT(run()));
    connect(resetBouton,SIGNAL(clicked(bool)), this, SLOT(reset()));
    connect(sauveBouton,SIGNAL(clicked(bool)), this, SLOT(save()));

    //suppression des boutons qui ne servent plus:
    choix_depart_HLayout->removeWidget(Indication_Label);
    Indication_Label->setVisible(false);
    choix_depart_HLayout->removeWidget(aleatoire_Bouton);
    aleatoire_Bouton->setVisible(false);
    etat_precedent_Bouton->setVisible(false);
    simulerBouton->setVisible(false);
    go_Bouton->setVisible(false);
    foret_complete_FDF_Bouton->setVisible(false);
}

void FenetreChargement::etatSuivant()
{
    const Automate& a =simulateur->getAutomate();
    unsigned int taille = tailleSpinBox->value();
    unsigned int hauteur = taille;
    if(a.getDimension()==1)
        hauteur=1;
   // on applique la transition
    simulateur->next();
   // on récupère le dernier état
   Etat& etat = simulateur->dernier();
   // on l'affiche
   afficherEtat(etat, taille,hauteur);
}

void FenetreChargement::run()
{
     connect(timer, SIGNAL(timeout()), this, SLOT(afficherEtatSlot()));//Toutes les secondes (divisées par la vitesse), on active le slot.
     timer->start(1000/vitesseSlider->value());
}


void FenetreChargement::afficherEtatSlot(){
    connect(pauseBouton,SIGNAL(clicked(bool)), this, SLOT(pause()));
    const Automate& a =simulateur->getAutomate();
    unsigned int taille = tailleSpinBox->value();
    unsigned int hauteur = taille;
    if(a.getDimension()==1)
        hauteur=1;
    simulateur->next();
    afficherEtat(simulateur->dernier(), taille, hauteur);
}

void FenetreChargement::afficherEtat(Etat& etat, unsigned int taille, unsigned int hauteur)
{
    AutomateManager& manager = AutomateManager::getAutomateManager(); //on récupère l'AM
    AutomateManager::IteratorManager it = manager.getIteratorManager();
    //récupération du nom de l'automate voulu
    std::string nom = ((choixAutomate->currentText())).toStdString();
    while((it.current().getNomAutomate())!=nom)
       it.next();
    Automate& a = it.current(); //on recupere l'automate selectionné
    std::string type = a.getType();

    //On parcourt chaque cellule
    for(unsigned int j = 0; j<hauteur; ++j)
    {
       for(unsigned int i = 0; i < taille; ++i)
       {
         //Les couleurs specifiques du FDF -------------------------------------------------------
           if(type=="FDF"){
               if (etat.getCellule(i,j) == 0) { //Si l'etat est 0
                   grilleEtat->item(j, i)->setText(QString::number(0)); //On met 0 dans la cellule
                   grilleEtat->item(j, i)->setBackgroundColor(Qt::GlobalColor(3)); //On colore en blanc
                   grilleEtat->item(j, i)->setTextColor(Qt::GlobalColor(3));
                }
               if (etat.getCellule(i,j) == 1) { //Si l'etat est 1
                   grilleEtat->item(j, i)->setText(QString::number(1)); //On met 1 dans la cellule
                   grilleEtat->item(j, i)->setBackgroundColor("green"); //On colore en vert
                   grilleEtat->item(j, i)->setTextColor("green");
               }
               if (etat.getCellule(i,j) == 2) { //Si l'etat est 2
                   grilleEtat->item(j, i)->setText(QString::number(2)); //On met 2 dans la cellule
                   grilleEtat->item(j, i)->setBackgroundColor("grey"); //On colore en gris
                   grilleEtat->item(j, i)->setTextColor("grey");
               }
               if (etat.getCellule(i,j) == 3) { //Si l'etat est 3
                   grilleEtat->item(j, i)->setText(QString::number(3)); //On met 3 dans la cellule
                   grilleEtat->item(j, i)->setBackgroundColor("red"); //On colore en red
                   grilleEtat->item(j, i)->setTextColor("red");
               }
        }
        //Les couleurs standards pour un automate---------------------------------------------
          else{
               if (etat.getCellule(i,j) == 0) { //Si l'etat est 0
                   grilleEtat->item(j, i)->setText(QString::number(0)); //On met 0 dans la cellule
                   grilleEtat->item(j, i)->setBackgroundColor(Qt::GlobalColor(3)); //On colore en blanc
                   grilleEtat->item(j, i)->setTextColor(Qt::GlobalColor(3));
               } else if (etat.getCellule(i,j) == 1) {//Idem pour le noir
                   grilleEtat->item(j, i)->setText(QString::number(1));
                   grilleEtat->item(j, i)->setBackgroundColor(Qt::GlobalColor(2));
                   grilleEtat->item(j, i)->setTextColor(Qt::GlobalColor(2));
               }else {// Sinon, on place le numero de l'état dans la cellule et on colore en fonction de celui-ci
                   grilleEtat->item(j, i)->setText(QString::number(etat.getCellule(i,j)));
                   grilleEtat->item(j, i)->setBackgroundColor(Qt::GlobalColor(etat.getCellule(i,j)+2));
                   grilleEtat->item(j, i)->setTextColor(Qt::GlobalColor(etat.getCellule(i,j)+2));
               }
           }
       }
    }
}

void FenetreChargement::pause()
{
    disconnect(timer, SIGNAL(timeout()), this, SLOT(afficherEtatSlot()));
}

void FenetreChargement::reset()
{
    Etat& e = simulateur->getEtatDepart();
    simulateur->reset();
    const Automate& a =simulateur->getAutomate();
    unsigned int taille = tailleSpinBox->value();
    unsigned int hauteur = taille;
    if(a.getDimension()==1)
        hauteur=1;
    afficherEtat(e, taille, hauteur);
}


void FenetreChargement::save()
{
    Sauvegarde_etat("automates.xml",simulateur->getAutomate().getNomAutomate() , simulateur->dernier());
    QMessageBox::information(this,"Information","Enregistrement effectué");
}

void FenetreChargement::foret_complete_FDF(){
    unsigned int taille = tailleSpinBox->value();
    for (unsigned int j=0; j<taille;++j)
    {
        for (unsigned int i=0; i<taille;++i)
        {
                grilleEtat->item(j, i)->setText(QString::number(1)); //On met 1 dans la cellule
                grilleEtat->item(j, i)->setBackgroundColor("green"); //On colore en vert
                grilleEtat->item(j, i)->setTextColor("green");
        }
    }

}

int FenetreChargement::chargementAutomates(const QString str){
    AutomateManager& manager = AutomateManager::getAutomateManager();
    QDomDocument document;
    //Chargement du fichier
    QFile file(str);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        throw AutoCellException("Erreur overture du fichier");
        return -1;
    }
    if(!document.setContent(&file)){
        throw AutoCellException("Erreur set content");
        return -1;
    }
    file.close();

    //On recupere l'element racine
    QDomElement root = document.firstChildElement();

    //Parcours des noeuds
    QDomNodeList items = root.elementsByTagName("Automate");
    for(unsigned int i=0;i<items.count();i++){
        QDomNode itemNode = items.at(i);
        if(itemNode.isElement()){
            QDomElement automate = itemNode.toElement();//Conversion d'un noeud en element
            //On crée un specifier qui récupere les attibuts de l'automate
            AutomateSpecifier specifier((automate.attribute("Nom")).toStdString(), (automate.attribute("nbVoisinsMin")).toInt(), (automate.attribute("nbVoisinsMax")).toInt(), (automate.attribute("nbVoisinsVie")).toInt(),(automate.attribute("NumBitRegle")).toStdString(), (automate.attribute("NumRegle")).toInt(), (automate.attribute("ventNord")).toInt(), (automate.attribute("ventSud")).toInt(), (automate.attribute("ventEst")).toInt(), (automate.attribute("ventOuest")).toInt());
            //####Completer argument si nouveau type d'automate
            manager.creerAutomate(automate.attribute("Type").toStdString(), specifier);
        }
    }
}

void FenetreChargement::Sauvegarde_etat(const std::string &fichier, const std::string& name, const Etat& e)
{
    QDomDocument document("xml");
    QFile file(QString::fromStdString(fichier));//Ouverture du fichier XML
    //Vérification de l'ouverture du fichier en lecture
    if(!file.open(QIODevice::ReadOnly)){
        throw AutoCellException("Erreur ouverture du fichier");
     }
    //Vérification de l'attribution du fichier à l'objet QDomDocument
    if(!document.setContent(&file)){
      throw AutoCellException("Erreur set content");
    }
    file.close();
    //renvoie la racine
    QDomElement automates = document.documentElement();
    //Renvoie la 1ere balise "Automate"
    QDomNode automate = automates.firstChild();
    unsigned int taille = e.getTaille();
    unsigned int nb_ligne;
    if (e.getDimension()==1)
    {
        nb_ligne = 1;
    }
    else
    {
        nb_ligne = taille;
    }
    //parcours des balises "automate" pour trouver l'automate voulu
    while(!automate.isNull())
    {
        QDomElement elm = automate.toElement();
        if (!elm.isNull())
        {
            //Si l'automate courant est celui recherché
            if (elm.attribute("Nom") == QString::fromStdString(name))
            {
                //si balise Etat déjà présente : modification
                if (elm.firstChild().toElement().tagName() == "Etat")
                {
                    elm.removeChild(elm.firstChild());
                }
                    elm.setAttribute("Taille",taille);
                    QDomElement etat = document.createElement("Etat");//création balise Etat
                    elm.appendChild(etat);//filiation de la balise Etat
                    for (unsigned int l=0;l<nb_ligne;l++)
                    {
                        QDomElement ligne = document.createElement("Ligne");//création balise Ligne
                        etat.appendChild(ligne);//filiation de la balise Ligne
                        for (unsigned int c=0;c<taille;c++)
                        {
                            QDomElement cellule = document.createElement("Cellule");//création balise Cellule
                            ligne.appendChild(cellule);//filiation de la balise Cellule
                            cellule.setAttribute("Value",e.getCellule(c,l)); //chaque cellule a un attribut "value" contenant son état
                        }
                    }
            }
        }
        automate = automate.nextSibling();
    }

    //ouverture du fichier en écriture
    QFile file2(QString::fromStdString(fichier));
    if (!file2.open(QIODevice::WriteOnly))
        throw AutoCellException("Erreur ouverture du fichier");

    //sauvegarde de l'état de l'automate dans le fichier
    QTextStream stream (&file2);
    stream << document.toString();
    file2.close();
}

Etat FenetreChargement::Chargement_dernier_etat(const std::string& fichier, const Automate &a)
{
    QDomDocument document("xml");
    QFile file(QString::fromStdString(fichier));//Ouverture du fichier XML
    //Vérification de l'ouverture du fichier en lecture
    if(!file.open(QIODevice::ReadOnly)){
        throw AutoCellException("Erreur ouverture du fichier");
     }
    //Vérification de l'attribution du fichier à l'objet QDomDocument
    if(!document.setContent(&file)){
      throw AutoCellException("Erreur set content");
    }
    file.close();
    unsigned int l = 0;
    unsigned int taille,nb_ligne;
    unsigned int automate_trouve = 0;
    std::vector<std::vector<int>> old_state;//Tableau dynamique qui va récupérer l'état de chaque cellule enregistré dans le fichier

    //variable fonctionnant comme un tableau contenant les valeurs des cellules d'une ligne
    QDomNodeList tab;

    //récupération de la racine
    QDomElement automates = document.documentElement();

    //Récupération de la 1ere balise "Automate"
    QDomNode automate = automates.firstChild();

    //parcours des balises "automate" pour trouver l'automate voulu
    while(!automate.isNull())
    {
        //Conversion du noeud en un élément
        QDomElement elm = automate.toElement();
        if (!elm.isNull())
        {
            //Si l'automate courant est celui recherché
            if (elm.attribute("Nom") == QString::fromStdString(a.getNomAutomate()))
            {
                automate_trouve = 1; //indique que l'automate recherché a été trouvé
                //Récupération de la taille (correspondant au nombre de cellules par ligne)
                taille = elm.attribute("Taille").toInt();
                //Récupération du nombre de lignes de l'état
                if (a.getDimension()==1)
                    nb_ligne = 1;
                else
                    nb_ligne = taille;

                //Agrandissement du tableau old_state pour récupérer les états de toutes les cellules enregistrées
                for (unsigned int i=0; i<nb_ligne; i++)
                    old_state.push_back(std::vector<int>(taille));

                //récupération de la balise Etat
                QDomElement etat = elm.firstChild().toElement();

                //Vérification de l'existence d'un état dans le fichier
                if (etat.isNull())
                {
                    throw AutoCellException("Pas d'état enregistré pour cet automate");
                }
                while(!etat.isNull())//parcours de la balise "Etat"
                {
                    QDomElement ligne = etat.firstChild().toElement();
                    //parcours de chaque ligne
                    while(!ligne.isNull())
                    {
                        //tab récupère toutes les balises "cellule" filles de la balise "ligne"
                        tab = ligne.childNodes();
                        //Récupération de l'état de chaque cellule de la ligne dans le tableau old_state
                        for (unsigned int c=0; c<tab.length();c++)
                            old_state[l][c]=tab.item(c).toElement().attribute("Value").toInt();
                        ligne = ligne.nextSibling().toElement();
                        l++;
                    }
                    etat=etat.nextSibling().toElement();
                }

            }

        }
        automate = automate.nextSibling();
    }
    if (automate_trouve == 0)
            throw AutoCellException("Aucun automate porte le nom passé en paramètre");
    //Préparation de l'état renvoyé par la fonction
    Etat e(taille,a);
    for (unsigned int k=0; k<nb_ligne; k++ )
    {
        for (unsigned int p=0; p<taille; p++)
            e.setCellule(old_state[k][p],p,k);//Attribution de l'état de chaque cellule dans le tableau "valeur"
    }
    return e;
}
