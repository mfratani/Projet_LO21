#include "chargementautomates.h"
#include "autocellexception.h"
#include "automate.h"
#include "JeuDeLaVie.h"
#include "automatesManager.h"
#include "automatespecifier.h"
#include <QtXml>


int chargementAutomates(const QString str){

    AutomateManager& manager = AutomateManager::getAutomateManager();
    QDomDocument document;

    //Chargement du fichier
    QFile file(str);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        throw AutoCellException("Erreur overture du fichier");
        return -1;
    }

    if(!document.setContent(&file)){
        throw AutoCellException("Erreur set content");
        return -1;
    }
    file.close();

    //On recupere l'element racine

    QDomElement root = document.firstChildElement();

    //Parcours des noeuds
    QDomNodeList items = root.elementsByTagName("Automate");

    for(unsigned int i=0;i<items.count();i++){
        QDomNode itemNode = items.at(i);


        if(itemNode.isElement()){

            QDomElement automate = itemNode.toElement();//Conversion d'un noeud en element

            //On crée un specifier qui récupere les attibuts de l'automate
            AutomateSpecifier specifier((automate.attribute("Nom")).toStdString(), (automate.attribute("nbVoisinsMin")).toInt(), (automate.attribute("nbVoisinsMax")).toInt(), (automate.attribute("nbVoisinsVie")).toInt(),(automate.attribute("NumBitRegle")).toStdString(), (automate.attribute("NumRegle")).toInt(), (automate.attribute("ventNord")).toInt(), (automate.attribute("ventSud")).toInt(), (automate.attribute("ventEst")).toInt(), (automate.attribute("ventOuest")).toInt());
            //####Completer argument si nouveau type d'automate

            //Création d'un automate selon le type
            if(automate.attribute("Type") == "JDV"){
                manager.creerAutomate(JDV, specifier);
            }
            else if(automate.attribute("Type") == "D1"){
                manager.creerAutomate(D1, specifier);
            }
            else if(automate.attribute("Type")=="FDF"){
                manager.creerAutomate(FDF, specifier);
            }
            //############Inserer code pour créer un nouveau type d'automate############################
        }
    }
}
